package cliffberg.flow.analyzer;

import cliffberg.flow.parser.FlowParser.Arg_sectionContext;
import cliffberg.flow.parser.FlowParser.Value_defContext;
import cliffberg.flow.parser.FlowParser.Method_signatureContext;
import cliffberg.flow.parser.FlowParser.Method_return_typeContext;
import cliffberg.flow.parser.FlowParser.Arg_def_seqContext;
import cliffberg.flow.parser.FlowParser.Arg_defContext;
import cliffberg.flow.parser.FlowParser.Act_arg_seqContext;
import cliffberg.flow.parser.FlowParser.Act_argContext;
import cliffberg.flow.parser.FlowParser.Value_type_specContext;

import cliffberg.symboltable.SymbolEntry;
import cliffberg.symboltable.NameScope;
//import cliffberg.symboltable.ValueType;
import cliffberg.symboltable.NameResolution;
import cliffberg.symboltable.VisibilityChecker;
import cliffberg.symboltable.DeclaredEntry;
import cliffberg.symboltable.TypeDescriptor;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class VTableFactory {

	private NameResolution<FlowValueType, ParseTree, ParseTree, ParseTree, TerminalNode> nameResolver;
	private VisibilityChecker visibilityChecker;
	private TypeSpecAnalyzer typeSpecAnalyzer;

	public VTableFactory(NameResolution<FlowValueType, ParseTree, ParseTree, ParseTree, TerminalNode> nameResolver,
		TypeSpecAnalyzer typeSpecAnalyzer,
		VisibilityChecker visibilityChecker) {

		this.nameResolver = nameResolver;
		this.typeSpecAnalyzer = typeSpecAnalyzer;
		this.visibilityChecker = visibilityChecker;
	}

	/**
	 Algorithm VT-1. See Analyzer Design.
	 Overview: Identify the set of function or method defs that might match the specified name and
	 static types of the actual argument expressions. Construct and return a table containing those matches.
	 'assignValueDef' may be null.
	 */
	public List<VTableEntry> createVTable(String name, List<NameScope<FlowValueType, ParseTree, TerminalNode>> searchScopes,
		Arg_sectionContext actArgSection, Value_defContext assignValueDef, boolean searchParentScopes) {

		/* Search each of the scopes in SearchScopes for method definitions named Tident.
			(Search appended scopes, but do not search parent scopes.)  Add each match uniquely
			to nameMatchEntries. */

		List<SymbolEntry<FlowValueType, ParseTree, TerminalNode>> nameMatchEntries = new LinkedList<SymbolEntry<FlowValueType, ParseTree, TerminalNode>>();
		for (NameScope<FlowValueType, ParseTree, TerminalNode> scope : searchScopes) {
			if (searchParentScopes)
				nameMatchEntries.addAll(nameResolver.identifySymbols(name, scope, this.visibilityChecker));
			else
				nameMatchEntries.addAll(nameResolver.identifySymbolsLocally(name, scope));
		}

		/* Create the v-table. Ref. Analyzer Design, "V-Table Design":
			*/

		List<VTableEntry> vTableEntries = new LinkedList<VTableEntry>();
		next_method_entry: for (SymbolEntry<FlowValueType, ParseTree, TerminalNode> e : nameMatchEntries) {

			if (! (e instanceof DeclaredEntry)) {
				throw new RuntimeException("Entry " + e.getName() + " is not a DeclaredEntry");
			}

			ParseTree n = ((DeclaredEntry<FlowValueType, ParseTree, TerminalNode>)e).getDefiningNode();
			if (! (n instanceof Method_signatureContext)) {
				throw new RuntimeException("ParseTree is not a method_signature: it is a " + n.getClass().getName());
			}
			Method_signatureContext methodSig = (Method_signatureContext)n;

			/* Obtain the formal return type and compare with any assignment. Note that
				overloaded methods must define the same return type. */

			FlowTypeDescriptor returnTypeDesc = null;
			Method_return_typeContext returnType = methodSig.method_return_type(); // may be null

			if (assignValueDef != null) {

				// Check that the method definition specifies a return type:
				if (returnType == null) {
					addLinePosExc(assignValueDef, "That method does not return a value");
					continue next_method_entry;
				}

				/* Verify that the method def result type is assignable to the target of the assignment.
					method_return_type : value_type_spec | conduit_ret_type_spec ;
					*/

				if (returnType.value_type_spec() != null) {
					/* Perform type analyzis of the return type: */
					try { this.typeSpecAnalyzer.analyzeTypeSpec(returnType.value_type_spec()); }
					catch (Exception ex) {
						addLinePosExc(returnType, ex.getMessage());
						continue next_method_entry;
					}

					returnTypeDesc = (FlowTypeDescriptor)
						(this.nameResolver.getState().getTypeDesc(returnType.value_type_spec()));

				} else if (returnType.conduit_ret_type_spec() != null) {
					/* TBD: Verify that the method is private:.... */

					/* Perform type analyzis of the return type. It must be a ConduitTypeDescriptor.
					 	See LRM, the section "Private Methods May Return Conduits". */
					try {
						returnTypeDesc =
							this.typeSpecAnalyzer.analyzeCondRetTpSpec(returnType.conduit_ret_type_spec());

					} catch (Exception ex) {
						addLinePosExc(returnType.conduit_ret_type_spec(), ex.getMessage());
						continue next_method_entry;
					}

				} else {
					throwLinePosExc(returnType, "Unexpected production");
				}
			}

			boolean isVariadic = false;

			/* Argument checking: */

			Arg_def_seqContext argDefSeq = methodSig.method_signature_scope().arg_def_seq(); // may be null
			List<Arg_defContext> argDefs = // never null
				(argDefSeq == null ? new LinkedList<Arg_defContext>() : argDefSeq.arg_def());
			int noOfFormals = argDefs.size();

			Act_arg_seqContext actArgSeq = actArgSection.act_arg_seq(); // may be null
			List<Act_argContext> actArgs = // never null
				(actArgSeq == null ? new LinkedList<Act_argContext>() : actArgSeq.act_arg());
			int noOfActuals = actArgs.size();

			/* Check if there are no formals, but there are actuals: */
			if (noOfFormals == 0 && noOfActuals > 0) continue next_method_entry;  // not a match

			/* Determine if variadic: */
			if (argDefSeq.variadic_def() != null) {
				if (argDefs.size() == 0) {
					addLinePosExc(argDefSeq, "Variadic arg list must have at least one argument");
					continue next_method_entry;
				}

				isVariadic = true;

				// Check that there are at least n-1 actuals:
				if (noOfFormals - 1 > noOfActuals) continue next_method_entry;

			} else { // not variadic
				// Check there are enough actual args: no of formals must equal no of actuals
				if (noOfFormals != noOfActuals) continue next_method_entry;
			}

			/* First iterate over non-variadic formals:
				When a variadic formal is reached, break.
				*/

			DeclaredEntry[] argEntries = new DeclaredEntry[argDefs.size()];
			FlowTypeDescriptor[] argTypes = new FlowTypeDescriptor[argDefs.size()];
			Iterator<Arg_defContext> argDefIter = argDefs.iterator();
			Iterator<Act_argContext> actArgIter = actArgs.iterator();

			int argDefIndex = -1;
			next_arg_def: for (;;) { // for each formal arg

				Arg_defContext argDef = null;
				Act_argContext actArg = null;

				/* Iterate through formal and actual args, accounting for variadic args.
					Note that we have already checked that there are enough actual arguments
					for the formals, and that there are not too many actuals. */

				// Get next formal:
				try {
					argDef = argDefIter.next();
					argDefIndex++;
					argEntries[argDefIndex] = this.nameResolver.getState().getDef(argDef);

				} catch (NoSuchElementException ex) {
					// no more formals
				}

				// Get next actual:
				try {
					actArg = actArgIter.next();

				} catch (NoSuchElementException ex) {
					break next_arg_def; // no more actuals
				}

				// Perform early type analysis of the argument:
				Value_type_specContext argTypeSpec = argDef.value_decl().value_type_spec(); // may be null
				if (argTypeSpec == null) {
					addLinePosExc(argDef, "Missing type spec for argument");
					continue next_method_entry;
				}
				FlowTypeDescriptor argTypeDesc = null;
				try {
					TypeDescriptor<FlowValueType> td = this.typeSpecAnalyzer.analyzeTypeSpec(argEntries[argDefIndex], argTypeSpec);
					if (! (td instanceof FlowTypeDescriptor)) throwLinePosExc(argDef, "type descriptor is not a FlowTypeDescriptor");
					argTypeDesc = (FlowTypeDescriptor)td;
				} catch (Exception ex) {
					addLinePosExc(argDef, ex.getMessage());
					continue next_method_entry;
				}

				/* Determine the type desc of the actual arg.
					act_arg : expr | inline_func ;
					*/
				FlowTypeDescriptor actArgTypeDesc = null;
				{
					TypeDescriptor<FlowValueType> td = null;
					if (actArg.expr() != null) {
						td = this.nameResolver.getState().getTypeDesc(actArg.expr());
					} else if (actArg.inline_func() != null) {
						td = this.nameResolver.getState().getTypeDesc(actArg.inline_func());
					} else throwLinePosExc(actArg, "Unexpected production");

					if (! (td instanceof FlowTypeDescriptor)) throwLinePosExc(actArg, "type descriptor is not a FlowTypeDescriptor");
					actArgTypeDesc = (FlowTypeDescriptor)td;
				}

				/* Verify that the actual type is automatically convertible to the formal type: */
				if (actArgTypeDesc == null) {
					addLinePosExc(actArg, "Unable to determine type");
					continue next_method_entry;
				}
				if (! actArgTypeDesc.canBeAssignedTo(argTypeDesc)) continue next_method_entry;

				argTypes[argDefIndex] = (FlowTypeDescriptor)argTypeDesc;
			}

			/* Create v-table entry: */

			VTableEntry vTableEntry = new VTableEntry(e, isVariadic, argEntries, argTypes, returnTypeDesc);
			vTableEntries.add(vTableEntry);
		}

		return vTableEntries;
	}

	private void throwLinePosExc(ParseTree node, String msg) {
		this.nameResolver.getState().throwLinePosExc(node, msg);
	}

	private void addLinePosExc(ParseTree node, String msg) {
		this.nameResolver.getState().addLinePosExc(node, msg);
	}
}
