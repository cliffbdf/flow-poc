package cliffberg.flow.analyzer;

import cliffberg.symboltable.Analyzer;
import cliffberg.symboltable.AnalyzerFactory;
import cliffberg.symboltable.CompilerState;
import cliffberg.symboltable.ImportHandler;
import cliffberg.symboltable.NamespaceProcessorBase;

import org.antlr.v4.runtime.tree.ParseTree;

import java.io.Reader;

public class FlowAnalyzerFactory<Type, Node extends ParseTree, Start extends Node,
	AOidRef extends Node, TId extends Node>
	implements AnalyzerFactory {

	public FlowAnalyzerFactory(String bindingFilePath, int maxNoOfErrors) {
		this(new FlowCompilerState(maxNoOfErrors), bindingFilePath);
	}

	public FlowAnalyzerFactory(CompilerState state, String bindingFilePath) {
		this.state = state;
		this.bindingFilePath = bindingFilePath;
	}

	public CompilerState getCompilerState() { return this.state; }

	public NamespaceProcessorBase createNamespaceProcessor() {
		return new FlowNamespaceProcessor(createAnalyzer(state));
	}

	public Analyzer createAnalyzer(CompilerState state) {
		if (! (state instanceof FlowCompilerState)) throw new RuntimeException(
			"Compiler state must be a FlowCompilerState");
		return new FlowAnalyzer<Type, Node, Start, AOidRef, TId>(
			(FlowCompilerState)state, (FlowImportHandler)(createImportHandler()), state.getMaxNoOfErrors());
	}

	public ImportHandler createImportHandler() {
		return new FlowImportHandler(this, this.bindingFilePath);
	}

	private CompilerState state;
	private String bindingFilePath;
}
