package cliffberg.flow.analyzer;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ParseTree;

import cliffberg.symboltable.SymbolWrapper;

import java.io.PrintStream;

import static cliffberg.utilities.AssertionUtilities.*;

public class FlowSymbolWrapper<Node> implements SymbolWrapper<Node> {
	
	public String getText(Node node) {
		ParseTree tree = asParseTree(node);
		if (tree instanceof TerminalNode) return tree.getText();
		else {
			int n = tree.getChildCount();
			String result = "";
			for (int i = 0; i < n; i++) {
				if (i > 0) result = result + " ";
				ParseTree child = tree.getChild(i);
				result = result + getText(asNode(child));
			}
			return result;
		}
	}
	
	public int getLine(Node node) {
		if (node instanceof TerminalNode)
			return ((TerminalNode)node).getSymbol().getLine();
		else {
			ParseTree firstChild = asParseTree(node).getChild(0);
			if (firstChild == null) throw new RuntimeException("No terminals found for non-terminal");
			return getLine(asNode(firstChild));  // recursive
		}
	}
	
	public int getPos(Node node) {
		if (node instanceof TerminalNode)
			return ((TerminalNode)node).getSymbol().getCharPositionInLine()+1;
		else {
			ParseTree firstChild = asParseTree(node).getChild(0);
			if (firstChild == null) throw new RuntimeException("No terminals found for non-terminal");
			return getPos(asNode(firstChild));  // recursive
		}
	}
	
	public static void printTree(ParseTree tree, PrintStream pr, int indentBy) {
		String spaces = getSpaces(indentBy);
		pr.print(spaces + tree.getClass().getName());
		if (tree instanceof TerminalNode) pr.println(": " + tree.getText());
		else pr.println();
		int n = tree.getChildCount();
		for (int i = 0; i < n; i++) {
			ParseTree child = tree.getChild(i);
			printTree(child, pr, indentBy+1);
		}
	}
	
	private static String spaces = "";
	
	private static String getSpaces(int level) {
		int len = spaces.length();
		for (int i = len+1; i <= level; i++) {
			spaces = spaces + ' ';
		}
		return spaces.substring(0, level);
	}
	
	private ParseTree asParseTree(Node node) {
		return (ParseTree)node;
	}
	
	private Node asNode(ParseTree pt) {
		return (Node)pt;
	}
}
