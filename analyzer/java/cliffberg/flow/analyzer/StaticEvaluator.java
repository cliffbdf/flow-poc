package cliffberg.flow.analyzer;

import static cliffberg.flow.analyzer.ParseTreeUtilities.*;
import static cliffberg.utilities.AssertionUtilities.*;

import cliffberg.flow.parser.FlowParser.*;
import static cliffberg.flow.analyzer.FlowDataType.*;

import cliffberg.symboltable.SymbolTable;
import cliffberg.symboltable.Annotation;
import cliffberg.symboltable.ExprAnnotation;
import cliffberg.symboltable.SymbolEntry;
import cliffberg.symboltable.DeclaredEntry;
import cliffberg.symboltable.TypeDescriptor;
import cliffberg.symboltable.SymbolWrapper;
import cliffberg.symboltable.ConversionException;
import cliffberg.symboltable.NameResolution;
import cliffberg.symboltable.NameScope;
import cliffberg.symboltable.VisibilityChecker;
import cliffberg.symboltable.ValueType;

import cliffberg.flow.parser.FlowLexer;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

/**
 For determining the type (type descriptor) of values and expressions.

 Also, evaluate values and expressions that are statically determinable (at compile time).
 If a value or expression cannot be statically evaluated, return null.

 It is not an error to be unable to determine the value. Thus, each call to an evaluate method should
 check for null.

 Preconditions:
	* A namespaces have been defined and organized into a hierarchy.
	* All symbol definitions have been attributed with symbol table entries, except for value definitions
		that are ambiguous. (See Analyzer Design, the section "Actions, By Pass", and the pass
		labeled "Identify value_defs that are declarations".)

 Side effects: these are added to the compiler state:
 	A list of "type ambiguous array literals". These are array literals (array_literal) whose type
 		is ambiguous because the array literal contains zero elements.
 	A list of dynamic type checkers that should be inserted by a generator.
 	A list of automatic conversions that should be inserted by a generator.

 Package-visible methods:
 	evaluate(ValueContext)
 	evaluate(ExprContext)
 */
public class StaticEvaluator {

	private FlowCompilerState state;
	private TypeSpecAnalyzer typeSpecAnalyzer;
	private NameResolution<FlowValueType, ParseTree, ParseTree, ParseTree, TerminalNode> nameResolver;
	private VisibilityChecker visibilityChecker;
	private Map<SymbolEntry, Boolean> structTypeFieldsThatHaveDefaultValue = new HashMap<SymbolEntry, Boolean>();
	private Set<ParseTree> evaluatedMap = new TreeSet<ParseTree>();

	StaticEvaluator(FlowCompilerState state,
		TypeSpecAnalyzer typeSpecAnalyzer,
		NameResolution<FlowValueType, ParseTree, ParseTree, ParseTree, TerminalNode> nameResolver,
		VisibilityChecker visibilityChecker) {

		this.state = state;
		this.typeSpecAnalyzer = typeSpecAnalyzer;
		this.nameResolver = nameResolver;
		this.visibilityChecker = visibilityChecker;
	}

	/**
	 Attempt to evaluate the specified value. If unable to, return null.
	 */
	FlowValue evaluate(ValueContext value) throws Exception {

		if (checkEvaluated(value)) {
			Object obj = this.state.getVal(value).getValue();
			if (! (obj instanceof FlowValue)) throwLinePosExc(value, "Not a FlowValue");
			return (FlowValue)obj;
		}

		FlowValue result = null;
		TypeDescriptor<FlowValueType> typeDesc;
		boolean canBeStaticallyEvaluated = true; // starts true

		if (value.literal() != null) {  // can evaluate statically
			result = evaluate(value.literal());
			typeDesc = this.state.getTypeDesc(value.literal());
			this.state.setTypeDesc(value, typeDesc);

		} else if (value.qual_name() != null) {
			/* value : qual_name

			Static if the qualified name references a static value
			*/

			FlowValue qualNameValue = evaluate(value.qual_name());  // may be null

		} else if (value.qual_name_seq() != null) {
			/* value : qual_name_seq struct_init
			The value is a composite of the specified qual_names and the struct_init.

			Static if each of the qualified names reference static values, and if
			all of the struct elements are static.
			Semantic rule: the fields of each qual_name value do not conflict; but the struct_init
			may override fields of the qual_name values.
			*/

			/* Initialize a struct value; we will assemble the value by adding each struct
			that is referenced as a qual name, followed by the struct_init fields. */
			Map<String, FlowValue> fields = new HashMap<String, FlowValue>();
			result = new FlowStructValue(fields);

			// Iterate through each qual_name, and add that struct's fields to 'fields':
			Map<String, FlowValue> fields = new HashMap<String, FlowValue>();
			for (Qual_nameContext qualName : value.qual_name_seq()) {

				// static if the qualified name references a static value
				FlowValue qualNameValue = evaluate(qualName, fields);





				// Determine the type of the qual_name:
				TypeDescriptor<FlowValueType> qualNameTypeDesc = getTypeDesc(qualName);
				typeDesc = qualNameTypeDesc.duplicate(); // TypeDescriptor<FlowValueType>

				//
				if (qualNameTypeDesc.getType() != FlowStruct) {  // not a struct
					addLinePosExc(value.struct_init(),
						"FlowValueType " + getText(qualNameValue) +
						" is not a struct, so it cannot have a struct initialization");
					return null;
				}

				// Add the fields to the result:
				FlowStructValue qualNameStructValue = (FlowStructValue)qualNameValue;
				for (FlowValue qualNameFieldKey: qualNameStructValue.fields.keySet()) {
					FlowValue qualNameFieldValue = qualNameStructValue.fields.get(qualNameFieldKey);
					FlowValue resultFieldValue = result.get(qualNameFieldKey);

					if (resultFieldValue != null) {
						FlowTypeDescriptor qualNameFieldTypeDesc = qualNameFieldValue.getTypeDescriptor();
						FlowTypeDescriptor resultFieldTypeDesc = resultFieldValue.getTypeDescriptor();

						if (! resultFieldTypeDesc.equals(qualNameFieldTypeDesc)) { // type different
							addLinePosExc(qualName, "Struct field type conflict");
							return null;
						}
						if (! resultFieldValue.equals(qualNameFieldValue)) { // value different
							addLinePosExc(qualName, "Struct field value conflict");
							return null;
						}
					}
					result.put(qualNameFieldKey, qualNameFieldValue);
				}
			}

			/* Iterate over each field of the struct_init, and set the field value
				in the struct value. */

			FlowStructValue structValue = (FlowStructValue)qualNameValue;
			Struct_initContext structInit = value.struct_init();
			if (structInit != null) {

				Struct_elt_seqContext structEltSeq = structInit.struct_elt_seq();
				for (Struct_eltContext structElt : structEltSeq.struct_elt()) {

					/* From grammar:
						struct_elt	: value_def ( Tl_ar expr )? | function_def ;
						value_def	: ( Tvalue | Tconstant )? value_decl ;
						value_decl	: Tident ( Tcolon value_type_spec )? ;
						*/

					if (structElt.function_def() != null) continue;

					if (structElt.expr() != null) { // there is an initialization expression

						Value_defContext eltValueDef = structElt.value_def();

						DeclaredEntry<FlowValueType, ParseTree, TerminalNode> eltEntry = this.state.getDef(eltValueDef);
						if (eltEntry == null) { // a new field
							// Note that new fields can be added. Ref LRM § "Adding New Struct Fields".

							/* Note that the struct type is anonymous, and it has no symbol table entry,
								but it has a name scope. */

							NameScope anonNameScope = null;

							// Add the field to the instance's typeDesc:
							String initFieldName = getText(eltValueDef.value_decl().Tident());
							if (! (typeDesc instanceof StructExtensionTypeDescriptor)) {
								// First time through.
								// Re-define the struct type as an extension type:
								typeDesc = new StructExtensionTypeDescriptor(typeDesc);

								// Lazily create a name scope for the anonymous struct type:
								anonNameScope = new NameScope(value, null, this.state.getSymbolWrapper());
							}
							DeclaredEntry newEntry = new DeclaredEntry(initFieldName, anonNameScope, value);
							typeDesc.addedFields.add(newEntry);
						} else {
							....fieldInitializations.remove(eltEntry);  // the field has an initialization,
							// regardless of whether it is static
						}

						// Evaluate the field's initialization expression:
						FlowValue fieldValue = evaluate(structElt);
						if (fieldValue != null) {
							ExprContext expr = structElt.expr();
							if (expr == null) {
								canBeStaticallyEvaluated = false;
							} else {
								// Verify that the field value is compatible with the declared type, if
								// it was declared.
								if (eltEntry != null) {  // field is in the qual name
									TypeDescriptor entryTypeDesc = eltEntry.getTypeDesc();
									TypeDescriptor exprTypeDesc = state.getVal(expr).getTypeDesc();
									if (! exprTypeDesc.canBeAssignedTo(entryTypeDesc)) {
										addLinePosExc(structElt,
											"Expression type does not match the declared type");
										canBeStaticallyEvaluated = false;
									}
								}

								String initFieldName = getText(eltValueDef.value_decl().Tident());
								structValue.setField(initFieldName, fieldValue);
							}
						}
					} else {  // a field but no initialization expression
						// This is an error: a field is being added to a struct but
						// without an initial value:
						addLinePosExc(structElt, "No initial value provided for struct field");
						continue;
					}
				}
			}
			result = structValue;

		} else if (value.intrinsic_type_conv() != null) {
			// static if the value being converted is a static value
			result = evaluate(value.intrinsic_type_conv());
			typeDesc = this.state.getTypeDesc(value.intrinsic_type_conv());
			this.state.setTypeDesc(value, typeDesc);
		} else if (value.expr() != null) {
			// static if each of the expr args is static
			result = evaluate(value.expr());
			typeDesc = this.state.getTypeDesc(value.expr());
			this.state.setTypeDesc(value, typeDesc);
		} else if (value.if_expr() != null) {
			// static if the if expr is static, and the applicable case
			// expr is static
			result = evaluate(value.if_expr());
			typeDesc = this.state.getTypeDesc(value.if_expr());
			this.state.setTypeDesc(value, typeDesc);
		} else if (value.case_expr() != null) {
			// static if each expr arg is static
			result = evaluate(value.case_expr());
			typeDesc = this.state.getTypeDesc(value.case_expr());
			this.state.setTypeDesc(value, typeDesc);
		} else throw new RuntimeException(
			"\"" + getText(value) + "\" has an unexpected production");

		if (result != null) {
			ExprAnnotation annot = nameResolver.setExprAnnotation(
				value, result, typeDesc);
		}

		if (canBeStaticallyEvaluated) return result;
		else return null;
	}

	/**
	 Attempt to evaluate the specified expression. If unable to, return null.

	 expr					: expr array_spec  // for selecting array elements
	 						| logic_expr
							| value_type_assertion
							| expr Tperiod Tident // a struct field selection
							| value_def left_assign_op expr
							| expr right_assign_op value_def
							| function_call
	 */
	FlowValue evaluate(ExprContext expr) throws Exception {

		if (checkEvaluated(expr)) return this.state.getVal(expr).getValue();

		FlowTypeDesc typeDesc = null;
		FlowValue value = null;
		ExprAnnotation annot = null;

		if ((expr.right_assign_op() != null) || (expr.left_assign_op() != null)) {
			/* expr : value_def left_assign_op expr
					| expr right_assign_op value_def
				*/

			FlowValue subExprValue = evaluate(expr.expr());
			TypeDescription subExprTypeDesc = state.getTypeDesc(expr.expr());

			// Apply the type to the expr:
			this.state.setTypeDesc(subExprValue, subExprTypeDesc);

			/* We cannot determine the type of expr until we know the type of the value_def;
				but expr's type might be needed to determine the type of another value_def,
				so we need to determine expr's type before it can be used to determine another
				value_def's type. Thus, we need to resolve the type of the value_def now. */

			TypeDescription exprTypeDesc;

			Value_declContext valueDecl = expr.value_def().value_decl();
			if (valueDecl.value_type_spec() != null) {
				DeclaredEntry entry = state.getDef(expr.value_def());
				this.typeSpecAnalyzer.analyzeTypeSpec(entry, valueDecl.value_type_spec());
				exprTypeDesc = entry.getTypeDesc();

			} else {
				// There is no type declared for the value_def, so the type must be inferred
				// from the assignment: simply apply the sub-expr's type to the expr:
				exprTypeDesc = subExprTypeDesc;
			}

			typeDesc = exprTypeDesc;

			// Apply value to the expr, if possible:
			if (subExprValue != null) {
				if ( // a simpl assignment - not a computation
					(expr.left_assign_op() != null) && (expr.left_assign_op().Tl_ar() != null)
					||
					(expr.right_assign_op() != null) && (expr.right_assign_op().Tr_ar() != null)
					) {

					// we are able to statically detrmine the value of the subExprValue,
					// and the assign op is a pure assignment, so apply the value to expr:
					state.setExprAnnotation(expr, subExprValue, exprTypeDesc);

					value = subExprValue;
				}
			}

		} else if (expr.logic_expr() != null) {
			// expr : logic_expr

			Logic_exprContext logicExpr = expr.logic_expr();
			value = evaluate(expr.logic_expr());
			typeDesc = this.state.getTypeDesc(expr.logic_expr());

		} else if (expr.array_spec() != null) {
			// expr : expr array_spec  // for selecting array elements

			ExprContext baseExpr = expr.expr();
			Array_specContext arraySpec = expr.array_spec();

			FlowTypeDescriptor typeDesc = null;

			FlowValue baseExprValue = evaluate(baseExpr);
			FlowTypeDescriptor baseTypeDesc = this.state.getTypeDesc(baseExpr);
			if (baseTypeDesc == null) return null;

			// Verify that the base expression is an array:
			if (! (baseTypeDesc instanceof ArrayTypeDescriptor)) {
				addLinePosExc(expr, "Not an array");
				return null;
			}

			FlowTypeDesc elementTypeDesc = null;

			if (baseExprValue != null) {
				// To be a slice, the base value type must be an array:
				if (! (baseExprValue instanceof FlowArrayValue)) {
					addLinePosExc(arraySpec, "Expected an array value");
					return null;
				}

				if (! (baseTypeDesc instanceof ArrayTypeDescriptor)) {
					addLinePosExc(arraySpec, "Expected an array type");
					return null;
				}

				elementTypeDesc = ((ArrayTypeDesc)logicExprTypeDesc).elementTypeDesc;
			}

			boolean indexIsStatic = true;
			// Index indicates a slice.

			/* array_spec	: Tl_sqbr (
								  Tdotdot expr
								| expr Tdotdot
								| expr  // result is a single element - not an array
								| expr Tdotdot expr
								| Tactive  // only allowed for an elastic conduit
							)? Tr_sqbr
			*/

			FlowValue[] elements = null;
			List<ExprContext> rangeExprs = arraySpec.expr();

			if ((rangeExprs != null) && (rangeExprs.size() > 0)) {
				/* There is a range. */

				/* Identify the from and to expressions: */

				ExprContext fromExpr = null;
				ExprContext toExpr = null;

				ParseTree child1 = arraySpec.getChild(1);
				ParseTree child2 = arraySpec.getChild(2);
				ParseTree child2 = arraySpec.getChild(3);

				if ((child1 instanceof ExprContext) && (child3 instanceof ExprContext)) { // expr .. expr
					fromExpr = (ExprContext)child1;
					toExpr = (ExprContext)child3;
				} else if ((child1 instanceof TerminalNode) && (child2 instanceof ExprContext)) { // .. expr
					toExpr = (ExprContext)child2;
				} else if ((child1 instanceof ExprContext) && (child2 instanceof TerminalNode)) { // expr ..
					fromExpr = (ExprContext)child1;
				} else if (child1 instanceof ExprContext) { // expr
					fromExpr = (ExprContext)child1;
					toExpr = fromExpr;
				} else {
					throwLinePosExc(arraySpec, "Unexpected production");
				}

				/* Attempt to statically evaluate the from and to expressions: */

				FlowValue fromValue = null;
				if (fromExpr != null) fromValue = evaluate(fromExpr);

				FlowValue toValue = null;
				if (toExpr != null) toValue = evaluate(toExpr);

				/*  Validate the type of the index expr values: they must be int or long int: */

				if (fromExpr != null) {
					FlowTypeDescriptor fromTypeDesc = this.state.getTypeDesc(fromExpr);
					if (fromTypeDesc != null) {
						// Must be automatically convertible to long int:
						if (! fromTypeDesc.getType().automaticConversionToAllowed(FlowLongInt)) {
							addLinePosExc(fromExpr, "Must be convertible to long int");
						}
					}
				}

				if (toExpr != null) {
					FlowTypeDescriptor toTypeDesc = this.state.getTypeDesc(toExpr);
					if (toTypeDesc != null) {
						// Must be automatically convertible to long int:
						if (! toTypeDesc.getType().automaticConversionToAllowed(FlowLongInt)) {
							addLinePosExc(toExpr, "Must be convertible to long int");
						}
					}

					if (fromExpr == toExpr) { // selects one element - not a slice
						typeDesc = baseTypeDesc;
					}
				}

				/* If the index expr values are all static, verify that the range is legal: */

				if (fromValue != null) {
					try { fromValue = ((FlowUnsignedLongIntValue)
							(fromValue.convertTo(FlowDataType.FlowUnsignedLongInt))).value;
					} catch (ConversionException ex) {
						addLinePosExc(fromExpr, ex.getMessage() +
							"; array indices must have integral values");
						return null;
					}

					if (fromValue.value <= 0) {
						addLinePosExc(fromExpr, "Array indices must be positive");
						return null;
					}

					if (baseExprValue != null) {
						if (fromValue.value > ((FlowArrayValue)baseExprValue).elements.length) {
							addLinePosExc(fromExpr, "The from index is beyond the range of the array");
							return null;
						}
					}
				}

				if (toValue != null) {
					try { toValue = ((FlowUnsignedLongIntValue)
							(toValue.convertTo(FlowDataType.FlowUnsignedLongInt))).value;
					} catch (ConversionException ex) {
						addLinePosExc(toExpr, ex.getMessage() +
							"; array indices must have integral values");
						return null;
					}

					if (toValue.value <= 0) {
						addLinePosExc(toExpr, "Array indices must be positive");
						return null;
					}

					if (baseExprValue != null) {
						if (toValue.value > ((FlowArrayValue)baseExprValue).elements.length) {
							addLinePosExc(fromExpr, "The to index is beyond the range of the array");
							return null;
						}
					}
				}

				if ((fromValue != null) && (toValue != null)) {
					if (! (fromValue.value < toValue.value)) {
						addLinePosExc(toExpr, "Range must be ascending");
						return null;
					}
				}

				/* Compute the slice value, if it is statically determinable: */

				if ((baseExprValue != null) && (fromValue != null) && (toValue != null)) {

					long fromIndex = ((FlowUnsignedLongIntValue)fromValue).value;
					long toIndex = ((FlowUnsignedLongIntValue)toValue).value;

					if (fromValue == toValue) {
						// Value is the element - not an array:
						value = baseExprValue.elements[fromValue.value-1];
						typeDesc = baseTypeDesc;
					} else {
						FlowValue[] elements = Arrays.copyOfRange(value, fromIndex-1, toIndex);
						value = new FlowArrayValue(elements);
						typeDesc = new ArrayTypeDescriptor(baseTypeDesc, elements.size());
					}

				} else { // not able to statically evaluate
					typeDesc = new ArrayTypeDescriptor(baseTypeDesc);

					/* If a slice that we were not able to statically evaluate, then
						attribute the overall expr with a slice attribute, which specifies
						the range expressions, either or both of which may be null.
						*/
					if ((fromExpr != null) || (toExpr != null)) {
						annot = new SliceAnnotation(expr, baseExprValue, typeDesc, fromExpr, toExpr);
					}
				}

			} else if (arraySpec.Tactive() != null) {
				// Tactive
				/* Means that only the currently active element of the elastic conduit is to be included,
					but not the inactive ones.

					The parent expression (logic_expr) must reduce to the name of an elastic conduit.

					The runtime model requires that the 'active' symbol be replaced
					with a call that retrieves the dynamic array element
					that is active during the current method invocation. */

				/* Verify that the parent logic_expr reduces to the name of a conduit or object: */

				if (arraySpecs.size() != 1) {
					addLinePosExc(arraySpec.Tactive(),
						"'active' may only be used for an elastic conduit, which must have a single dimension");
					return null;
				}

				// Get the qualified name:
				Qual_nameContext qualName;
				try {
					// Retrieve using statically verificable parse tree method names, to verify
					// that the grammar has not changed since we coded this:
					qualName = logicExpr.comp_expr().num_expr().mult_expr().exp_expr().unop_expr().value().qual_name();

					// Check that each node has only one child:
					qualName = (Qual_nameContext)(
						getOnlyChild(
							getOnlyChild(
								getOnlyChild(
									getOnlyChild(
										getOnlyChild(
											getOnlyChild(
												getOnlyChild(logicExpr, Comp_exprContext),
											Num_exprContext),
										Mult_exprContext),
									Exp_exprContext),
								Unop_exprContext),
							ValueContext),
						Qual_nameContext));
				} catch (Exception ex) { // it doesn't reduce to a qualified name
					addLinePosExc(logicExpr,
						"'active' may only be used for the range of a conduit or object name");
					continue;
				}

				/* Verify that the qualified name is an elastic conduit. If it is,
					its definition must have been processed by now, and so we can resolve it: */

				SymbolEntry<FlowValueType, ParseTree, TerminalNode> entry =
					this.nameResolver.resolveSymbol(qualName.Tident(), getEnclosingScope(qualName),
						this.visibilityChecker);
				if (entry == null) {
					addLinePosExc(qualName, "Unable to identify conduit or object named " + qualName);
					return null;
				}

				// Verify that it is a conduit:
				ParseTree node = entry.getDefiningNode();
				if (! (node instanceof Conduit_specContext)) {
					addLinePosExc(qualName, "Only a conduit can use 'active'");
					return null;
				}

				// Verify that the conduit is declared to be elastic:
				if (((Conduit_specContext)node).elasticity() == null) {
					addLinePosExc(qualName, "Only an elastic conduit can use 'active'");
					return null;
				}

				// Not: The code generator will replace 'active' with a function call that returns
				// the active element.

				typeDesc = new ArrayTypeDescriptor(baseTypeDesc);

			} else {
				throwLinePosExc(arraySpec, "Unrecognized production");
			}

		} else if (expr.value_type_assertion() != null) {
			// expr : value_type_assertion
			Value_type_assertionContext valueTypeAssertion = expr.value_type_assertion();
			value = evaluate(valueTypeAssertion);
			typeDesc = state.getTypeDesc(valueTypeAssertion);

		} else if (expr.Tident() != null) {
			// expr : expr Tperiod Tident // a struct field selection
			ExprContext structExpr = expr.expr();
			FlowValue structValue = evaluate(structExpr);
			TypeDescription structTypeDesc = state.getTypeDesc(structExpr);
			if (! (structTypeDesc instanceof StructTypeDescriptor)) {
				addLinePosExc(structExpr, "Expected a struct typed expression, to select " + expr.Tident() + " from");
			} else {
				// Identify the ident in the context of the struct type:
				String fieldName = getText(expr.Tident());
				SymbolEntry<FlowValueType, ParseTree, TerminalNode> entry = structTypeDesc.symbolTable.getEntry(fieldName);

				// Analyze the ident's type. We need to do this proactively since it will not have
				// been done yet:
				try {
					typeDesc = this.typeSpecAnalyzer.analyzeEntryType(entry);
				} catch (Exception ex) {
					addLinePosExc(expr.Tident(), ex.getMessage());
				}

				// Obtain the field value from the struct:
				if (structValue != null) {
					value = structValue.fields.get(fieldName);
				}
			}

		} else if (expr.function_call() != null) {
			// expr : function_call ;
			// function_call : qual_name arg_section ;

			Function_callContext functionCall = expr.function_call();

			/* Resolve the qual_name and create a v-table for all possible matches: */

			List<NameScope<FlowValueType, ParseTree, TerminalNode>> searchScopes = new TreeSet<NameScope<FlowValueType, ParseTree, TerminalNode>>();
			searchScopes.add(getEnclosingScope(functionCall));

			List<VTableEntry> vTableEntries = VTableEntry.createVTable(
				functionCall.Tident().getText(), searchScopes,
				functionCall.arg_section(), null,
				true // search enclosing scopes as well
				);

			if (vTableEntries.size() == 0) {
				throwLinePosExc(id, "No matching method found");
			}

			// Annotate the function_call node:
			this.state.setVTableEntries(functionCall, vTableEntries);

		} else
			throwLinePosExc(expr, "Unexpected production");

		this.state.setTypeDesc(expr, typeDesc);
		if (annot == null) annot = new ExprAnnotation(expr, value, typeDesc);
		state.setVal(expr, annot);

		return value;
	}



	/*
	 Internal methods.
	 */


	/**
	 Add the specified node to the set of nodes that have been evaluated.
	 This enables us to avoid evaluating a node more than once, even if the
	 node was not able to produce a static value.
	 */
	private void setEvaluated(ParseTree node) {
		evaluatedMap.add(node);
	}

	/**
	 Return true if the specified node was visited for evaluation.
	 */
	private boolean hasBeenEvaluated(ParseTree node) {
		return evaluatedMap.contains(node);
	}

	/**
	 Convenience method: calls hasBeenEvaluated and then setEvaluated.
	 */
	private boolean checkEvaluated(ParseTree node) {
		if (hasBeenEvaluated(node)) return true;
		setEvaluated(node);
		return false;
	}

	private DeclaredEntry<FlowValueType, ParseTree, TerminalNode> getDef(ParseTree node) {
		return this.state.getDef(node);
	}

	private void setVal(ParseTree node, ExprAnnotation<FlowValueType, ParseTree> a) {
		this.state.setVal(node, a);
	}

	private TypeDescriptor<FlowValueType> getTypeDesc(ParseTree node) {
		return this.state.getTypeDesc(node);
	}

	private void addLinePosExc(ParseTree node, String msg) {
		this.state.addLinePosExc(node, msg);
	}

	private void throwLinePosExc(ParseTree node, String msg) {
		this.state.throwLinePosExc(node, msg);
	}

	private SymbolWrapper<ParseTree> getSymbolWrapper() { return this.state.getSymbolWrapper(); }

	private FlowSymbolWrapper getFlowSymbolWrapper() { return (FlowSymbolWrapper)(nameResolver.getSymbolWrapper()); }

	private String getText(ParseTree node) { return getFlowSymbolWrapper().getText(node); }

	private int getLine(ParseTree node) { return getFlowSymbolWrapper().getLine(node); }

	private int getPos(ParseTree node) { return getFlowSymbolWrapper().getPos(node); }



	/*
	 Internal methods, for evaluating sub-expressions.
	 */


	private FlowValue evaluate(LiteralContext literal) throws Exception {

		if (checkEvaluated(literal)) return this.state.getVal(literal).getValue();

		FlowValue result = null;

		if (literal.intrinsic_literal() != null) {
			Intrinsic_literalContext intrinsicLiteral = literal.intrinsic_literal();
			result = evaluate(intrinsicLiteral);
			ExprAnnotation annot = nameResolver.setExprAnnotation(
				literal, result, this.state.getTypeDesc(intrinsicLiteral));
		} else if (literal.array_literal() != null) {
			Array_literalContext arrayLiteral = literal.array_literal();
			result = evaluate(arrayLiteral);
			ExprAnnotation annot = nameResolver.setExprAnnotation(
				literal, result, this.state.getTypeDesc(arrayLiteral));
		} else
			throwLinePosExc(literal, "Unexpected production");

		return result;
	}

	private List<FlowValue> evaluate(Arg_sectionContext argSection) {

		List<FlowValue> argValues = new LinkedList<FlowValue>();
		for (Act_argContext actArg : argSection.act_arg_seq()) {
			if (actArg.inline_func() != null) {
				/* For simplicity, assume the arg inline function is non-static. */
				Value_type_specContext funcVts = actArg.inline_func().function_spec().value_type_spec();
				TypeDescriptor typeDesc = this.typeSpecAnalyzer.analyzeTypeSpec(null, funcVts);
				nameResolver.setExprAnnotation(actArg, null, typeDesc);
				argValues.add(null);

			} else if (actArg.expr() != null) {
				FlowValue argValue = evaluate(actArg.expr());
				nameResolver.setExprAnnotation(
					actArg, argValue, this.state.getTypeDesc(actArg));
				argValues.add(argValue);

			} else
				throwLinePosExc(actArg, "Unexpected production");
		}

		return argValues;
	}

	/**
	If fields is not null, merge the qualName's fields into the specified fields map.
	If a merge cannot occur due to a field name conflict, throw a line position exception.
	*/
	private FlowValue evaluate(Qual_nameContext qualName, Map<String, FlowValue> fields) throws Exception {

		/* qual_name :  Tident ( ( Tperiod | Tcolon ) Tident )* ;
			*/

		/* If the qual_name has a static value, set an ExprAnnotation and return the value.
			Regardless, set the typeSpec of the qual_name. If the qual_name is not resolvable,
			it is an error.
			*/
		NameScope<FlowValueType, ParseTree, TerminalNode> context = getEnclosingScope(qualName);
		if (context == null) {
			throwLinePosExc(qualName, "Internal error - could not identify enclosing scope");
		}
		SymbolEntry<FlowValueType, ParseTree, TerminalNode> entry = nameResolver.resolveSymbol(qualName.Tident(), context,
			this.visibilityChecker);

		if (entry == null) {
			addLinePosExc(qualName, "Unable to resolve");
			return null;
		}

		TypeDescriptor typeDesc = entry.getTypeDesc(); // may be null
		ParseTree defNode = entry.getDefiningNode();
		FlowValue value = (FlowValue)(this.state.getVal(defNode).getValue());
		if (value == null) {
			boolean canBeStaticallyEvaluated = true;

			// Determine the type of the qual_name:
			TypeDescriptor<FlowValueType> qualNameTypeDesc = getTypeDesc(value.qual_name());
			typeDesc = qualNameTypeDesc.duplicate(); // TypeDescriptor<FlowValueType>

			if (qualNameTypeDesc.getType() == FlowStruct) {  // a struct

				// Note: At this point, some fields of the value might be unset.

				value = new FlowStructValue(fields); // an empty struct value

				/* Keep track of which fields have been initialized: */
				Map<DeclaredEntry, Boolean> fieldUninitializations = new HashMap<DeclaredEntry, Boolean>();

				StructTypeDescriptor qualNameStructTypeDesc = (StructTypeDescriptor)qualNameTypeDesc;
					// Cast must succeed because the type is a struct.

				SymbolTable<FlowValueType, ParseTree, TerminalNode> qualNameSymbolTable =
					qualNameStructTypeDesc.symbolTable;
				Set<SymbolEntry<FlowValueType, ParseTree, TerminalNode>> entries = qualNameSymbolTable.getAllEntries();
					// This gets all the entries, including for any appended symbol tables.

				/*
					*/
				for (SymbolEntry<FlowValueType, ParseTree, TerminalNode> entry : entries) {
					if (this.structTypeFieldsThatHaveDefaultValue.get(entry) == null) {
						// we have not yet visited the entry
						// Determine if the type field has a default value. (If so, then the
						// struct_init (see below) does not need to specify a value for this field.)

						// Find the type field's initialization expression if any, and call evaluate on it.
						// Then add it to structTypeFieldsThatHaveDefaultValue:
						DeclaredEntry<FlowValueType, ParseTree, TerminalNode> dentry = (DeclaredEntry)entry;
						ParseTree defNode = dentry.getDefiningNode();
						if (defNode instanceof Value_defContext) {
							ParseTree parent = defNode.getParent();
							assertIsA(parent, Struct_eltContext.class);
							Struct_eltContext structElt = (Struct_eltContext)parent;
							if (structElt.expr() == null) {  // no default value
								structTypeFieldsThatHaveDefaultValue.put(entry, false);
							} else {
								FlowValue v = evaluate(structElt.expr());
								if (v == null) canBeStaticallyEvaluated = false;
								structTypeFieldsThatHaveDefaultValue.put(entry, true);
							}

							/* Add the field value to the struct value: */
							valueFields.put(....);

						} else
							continue;  // skip - we only care about fields that define values
					}

					if (! structTypeFieldsThatHaveDefaultValue.get(entry)) {
						// type def for field does not specify a default initial value
						fieldUninitializations.put(entry, Boolean.FALSE);
					}
				}

				if (fieldUninitializations.size() > 0) {
					for (ParseTree node : fieldUninitializations.keySet()) {
						addLinePosExc(node, "No initial value found");
					}
					canBeStaticallyEvaluated = false;
				}
			}

			if (! canBeStaticallyEvaluated) value = null;
			// Apply the struct_init value to the qual_name value:
			if (value != null) this.state.setVal(new ExprAnnotation(value, result, typeDesc));
			this.state.setTypeDesc(value.qual_name(), typeDesc);

		} else { // the quan_name was already annotated with a value
			if (fields != null) { // attempt to merge the qual_name fields into the fields argument
				....
				if (field name collision) throwLinePosExc(....);
			}
		}

		this.state.setTypeDesc(qualName, typeDesc);
		if (value != null) this.state.setVal(qualName, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Intrinsic_type_convContext intrinsicTypeConv) throws Exception {

		/*
			intrinsic_type_conv : intrinsic_type_spec Tl_paren expr Tr_paren ;

			intrinsic_type_spec
							: sign_modifier? size_modifier? Tint
							| size_modifier? Tfloat
							| Tbyte
							| Tboolean
							| Tglyph
							| Tstring
							;
			*/

		/* From the LRM, "FlowValueType Conversions",

			Allowed explicit (non-automatic) conversions are,
				Any numeric type to any other numeric type
				string to glyph or glyph to string
			*/

		/* Determine if an automatic conversion applies. If so, flag for insertion.
			If an automatic conversion is not allowed, determine if an explicit conversion
			is allowed. If so, flag for insertion.
			Otherwise, generate an error.
			*/

		Intrinsic_type_specContext intrinsicTypeSpec = intrinsicTypeConv.intrinsic_type_spec();
		ExprContext expr = intrinsicTypeConv.expr();

		TypeDescriptor exprTypeDesc = this.state.getTypeDesc(expr);
		if (exprTypeDesc == null) return null;

		FlowValue exprValue = evaluate(expr);

		FlowValueType fromType = (FlowDataType)(exprTypeDesc.getType());

		FlowDataType toType = this.typeSpecAnalyzer.analyzeIntrinsicType(intrinsicTypeSpec);

		if (fromType.automaticConversionToAllowed(toType)) {

		} else if (fromType.isNumeric() && toType.isNumeric()) {
			if (fromType.dataLossPossibleWhenConvertingTo(toType))
				addLinePosWarning(intrinsicTypeConv, "loss of data is possible");

			// Flag to insert converter
			this.state.addAutoTypeConverter(expr, fromType, toType);

		} else if ((fromType == FlowString) && (toType == FlowGlyph)) {

			addLinePosWarning(intrinsicTypeConv, "loss of data is possible");
			this.state.addAutoTypeConverter(expr, fromType, toType);

		} else if ((fromType == FlowGlyph) && (toType == FlowString)) {

			this.state.addAutoTypeConverter(expr, fromType, toType);

		} else if (fromType == toType) { // a no-op
		} else {
			addLinePosExc(intrinsicTypeConv, "Conversion not allowed");
			return null;
		}

		this.state.setTypeDesc(intrinsicTypeConv, typeDesc);
		if (value != null) this.state.setVal(intrinsicTypeConv, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Value_type_assertionContext valueTypeAssertion) throws Exception {

		/* 	value_type_assertion	: Tl_paren expr Tis ( qual_name | array_elt_type_spec array_def_spec+ )
										Tr_paren ; // for structs and arrays
			array_elt_type_spec		: intrinsic_type_spec | qual_name ;
			array_def_spec			: Tl_sqbr expr? Tr_sqbr ;  // if no expr, then the size is dynamic
			*/

		ExprContext expr = valueTypeAssertion.expr();
		Qual_nameContext assertedTypeQualName = valueTypeAssertion.qual_name();
		Array_elt_type_specContext assertedArEltTpSpec = valueTypeAssertion.array_elt_type_spec();
		List<Array_def_specContext> assertedArDefSpecs = valueTypeAssertion.array_def_spec(); // must have at least one

		/* The "is" assertion specifies that the expr is a struct that
			implements the asserted type specified by the qual_name. */

		// Find the name scope that encloses the assertion:
		NameScope<FlowValueType, ParseTree, TerminalNode> context = getEnclosingScope(valueTypeAssertion);
		if (context == null) {
			throwLinePosExc(valueTypeAssertion, "Internal error - could not identify enclosing scope");
		}

		// Evaluate the expression:
		FlowValue exprValue = evaluate(expr);

		/* Identify the asserted type: */

		TypeDescriptor assertedTypeDesc = null;

		if (assertedTypeQualName != null) {
			SymbolEntry<FlowValueType, ParseTree, TerminalNode> qualNameEntry =
				this.nameResolver.resolveSymbol(assertedTypeQualName.Tident(), context,
				this.visibilityChecker);

			if (qualNameEntry == null) {
				addLinePosExc(assertedTypeQualName, "Unable to identify");
				return null;
			}

			if (! (qualNameEntry.getDefiningNode() instanceof Type_defContext)) {
				addLinePosExc(assertedTypeQualName, "Not a type def");
				return null;
			}
			Type_defContext typeDef = (Type_defContext)(qualNameEntry.getDefiningNode());

			assertedTypeDesc = this.typeSpecAnalyzer.analyzeTypeSpec(
				qualNameEntry, typeDef.type_decl().value_type_spec());

		} else if (assertedArEltTpSpec != null) {
			if (assertedArDefSpec.size() == 0) throwLinePosExc(assertedArEltTpSpec,
				"Expected an array size");
			if (assertedArEltTpSpec == null) throwLinePosExc(assertedArEltTpSpec, "Unrecognized production");

			FlowTypeDescriptor assertedElementTypeDesc = this.typeSpecAnalyzer.analyzeTypeSpec(assertedArEltTpSpec);

			/* Assemble nested type descriptors - one nesting level for each array dimension: */
			assertedTypeDesc = assertedElementTypeDesc;
			// Iterate over dimensions, from the right-most dimension toward the left:
			for (int d = assertedArDefSpecs.size()-1; d >= 0; d--) {
				Array_def_specContext assertedArDefSpec = assertedArDefSpecs.get(d);
				Long size = null;
				ExprContext assertedArraySizeExpr = assertedArDefSpec.expr(); // may be null
				if (assertdArraySizeExpr != null) {
					FlowValue assertedArraySizeExprValue = evaluate(assertedArraySizeExpr);
					/* From LRM, "Arrays of Values":
						Array size, if specified (non-dynamic), the size must be statically determinable.
						*/
					if (assertedArraySizeExprValue == null) {
						addLinePosExc(assertedArraySizeExpr, "Array size, if specified, must be statically determinable");
						return null;
					}

					// Attempt to convert the asserted array size to a Long. The value may be 0 or
					// any positive long value:
					try {
						FlowValue nonNegLongValue = convertTo(FlowUnsignedLongInt);
					} catch (ConversionException ex) {
						addLinePosExc(assertedArraySizeExpr, ex.getMessage());
						return null;
					}

					size = nonNegLongValue.value;
				}

				assertedTypeDesc = new ArrayTypeDescriptor(assertedTypeDesc, size);
			}

		} else throwLinePosExc(valueTypeAssertion, "Unrecognized production");

		/* Verify that the target expression can be treated as the asserted type: */

		TypeDescriptor exprTypeDesc = this.state.getTypeDesc(expr);

		if (exprTypeDesc == null) {
			addLinePosExc(expr, "Indeterminate type");
			return null;
		}
		if (assertedTypeDesc == null) {
			addLinePosExc(valueTypeAssertion, "Asserted type is indeterminate");
			return null;
		}

		if (! assertedTypeDesc.isA(exprTypeDesc)) {
			addLinePosExc(expr, "The expression cannot be treated as the specified type");
			return null;
		}

		if (exprValue != null) {
			if (! exprTypeDesc.isA(assertedTypeDesc)) {
				addLinePosExc(expr, "The expression's static value is not of the specified type");
				return null;
			}
		}

		this.state.setTypeDesc(valueTypeAssertion, assertedTypeDesc);
		if (exprValue != null) this.state.setVal(valueTypeAssertion, exprValue, assertedTypeDesc);

		return exprValue;
	}

	private FlowValue evaluate(If_exprContext ifExpr) throws Exception {

		/* if_expr : Tif expr Tcomma? expr ( Telse expr ) ;  // first expr must be boolean
			*/

		TypeDescriptor typeDesc = null;
		FlowValue value = null;

		List<ExprContext> exprs = ifExpr.expr();

		ExprContext condExpr = exprs.get(0);
		ExprContext ifTrueExpr = exprs.get(1);
		ExprContext ifFalseExpr = exprs.get(2); // might be null

		FlowValue condExprValue = evaluate(condExpr);

		// Check if cond expr is boolean:
		TypeDescriptor condExprTypeDesc = this.state.getTypeDesc(condExpr);
		FlowDataType condExprType = condExprTypeDesc.getType();
		if (! (condExprType == FlowBoolean)) {
			addLinePosExc(condExpr, "Expected a boolean expression");
			return null;
		}

		if (condExprValue != null) { // the condition expr is statically determinable
			/* Discard the path that will never be used. */

			addLinePosWarn(condExpr, "Condition is a constant, so one path will never be executed");

			if (((FlowBooleanValue)(condExprValue)).value) { // keep the if-true expr

				value = evaluate(ifTrueExpr);
				typeDesc = this.state.getTypeDesc(ifTrueExpr);
				// Skip evaluation of the if-false expr
			} else { // keep the if-false expr

				value = evaluate(ifFalseExpr);
				typeDesc = this.state.getTypeDesc(ifFalseExpr);
				// Skip evaluation of the if-true expr
			}

		} else { // the condition expr is NOT statically determinable

			// Evaluate second and third expressions:
			FlowValue ifTrueExprValue = evaluate(ifTrueExpr);
			FlowValue ifFalseExprValue = evaluate(ifFalseExpr);

			// Find type that is common to each branch. Note that if the type is a
			// struct, it might have an alias, and it might implement multiple struct
			// extension types - and so its type might be anonymous and defined by more than one
			// base type.
			FlowTypeDescriptor commonTypeDesc = ifTrueExprValue.getTypeDesc().getCommonTypeDesc(ifFalseExprValue.getTypeDesc());
			if (commonTypeDesc == null) { // no common type found
				addLinePosExc(ifExpr, "Incompatible types in the if an else parts");
			} else {
				typeDesc = commonTypeDesc;
			}
		}

		this.state.setTypeDesc(ifExpr, typeDesc);
		if (value != null) this.state.setVal(ifExpr, value, typeDesc);
	}

	private FlowValue evaluate(Case_exprContext caseExpr) throws Exception {

		/* case_expr : Tcase expr Tl_curl_br case_expr_choice* ( Totherwise expr )? Tr_curl_br
			*/

		FlowValue value = null;
		FlowTypeDescriptor typeDesc = null;

		List<ExprContext> exprs = caseExpr.expr();
		ExprContext condExpr = exprs.get(0);
		ExprContext otherwiseExpr = exprs.get(1); // may be null

		FlowValue condExprValue = evaluate(condExpr);
		TypeDescriptor caseCondTypeDesc = this.state.getTypeDesc(condExpr);
		if (caseCondTypeDesc == null) return null;  // was not able to resolve the condition expr type

		FlowDataType caseCondType = caseCondTypeDesc.getType();

		// Evaluate each case expression:
		boolean allChoiceCondExprsAreStatic = true;
		Set<FlowValue> choiceCondExprValues = new TreeSet<FlowValue>();
		for (Case_expr_choiceContext ch : caseExpr.case_expr_choice()) {

			// case_expr_choice : expr Tcolon expr Tsemicolon

			FlowValue choiceCondExprValue = evaluate(ch.expr().get(0));
			if (choiceCondExprValue == null) allChoiceCondExprsAreStatic = false;

			if (allChoiceCondExprsAreStatic)
				choiceCondExprValues.add(choiceCondExprValue);

			FlowTypeDescriptor choiceCondTypeDesc = this.state.getTypeDesc();
			FlowDataType choiceCondType = choiceCondTypeDesc.getType();

			// Verify that the choice condition value type is compatible with the case condition type:
			if (! ((choiceCondType == caseCondType) || choiceCondTypeDesc.isA(caseCondTypeDesc))) {
				addLinePosExc(ch, "Incompatible type - must match type of the case condition expression");
				return null;
			}

			ExprContext chValueExpr = ch.expr().get(1);
			evaluate(chValueExpr);

			FlowTypeDescriptor chTypeDesc = this.state.getTypeDesc(chValueExpr);
			if (chTypeDesc == null) return null;
			typeDesc = chTypeDesc.getCommonTypeDesc(typeDesc);
			if (typeDesc == null) {
				addLinePosExc(caseExpr, "Some choices are not type compatible");
				return null;
			}
		}

		/* If each case choice is statically determinable, then every possible value in the range of
			the expression type must be covered or there must be an otherwise; otherwise,
			if not all of the case choices are statically determinable, then there must be an
			otherwise choice. */

		if (allChoiceCondExprsAreStatic) {
			if (otherwiseExpr == null) { /* every possible value in the range of
				the expression type must be covered */

				/* Check that every possible value in the expression type range is covered.
					This is only feasible for boolean and enum types: */

				if (caseCondType == FlowBoolean) {
					// there is at least one choice for each of the possible values of the boolean
					boolean foundTrue = false;
					for (FlowValue v : choiceCondExprValues) {
						if (((FlowBooleanValue)v).value == true) {
							foundTrue = true;
							break;
						}
					}
					if (! foundTrue) {
						addLinePosExc(caseExpr, "Missing 'true' case but there is no 'otherwise'");
						return null;
					}

					boolean foundFalse = false;
					for (FlowValue v : choiceCondExprValues) {
						if (((FlowBooleanValue)v).value == false) {
							foundFalse = true;
							break;
						}
					}
					if (! foundFalse) {
						addLinePosExc(caseExpr, "Missing 'false' case but there is no 'otherwise'");
						return null;
					}

				} else if (caseCondType == FlowEnum) {
					// there is at least one choice for each of the possible values of the enum
					for (TerminalNode id : ((EnumTypeDescriptor)caseCondTypeDesc).ids) { // each value of the enum type

						for (FlowValue v : choiceCondExprValues) {

							boolean found = false;
							String enumText = id.getSymbol().getText();
							if (enumText.equals(((FlowEnumValue)v).value)) {
								found = true;
								break;
							}
							if (! found) {
								addLinePosExc(caseExpr, "Missing " + enumText + " case but there is no 'otherwise'");
								return null;
							}
						}
					}

				} else {
					addLinePosExc(caseExpr, "case expression is missiong an 'otherwise' choice");
					return null;
				}
			}

		} else {
			if (otherwiseExpr == null) { // non-static choices but no 'otherwise' choice
				addLinePosExc(caseExpr, "If there are non-static choices, an otherwise must be included");
				return null;
			}
		}

		this.state.setTypeDesc(caseExpr, typeDesc);
		if (value != null) this.state.setVal(caseExpr, value, typeDesc);
	}

	private FlowValue evaluate(Intrinsic_literalContext intrinsicLiteral) throws Exception {

		/* intrinsic_literal	: Tint_literal
								| Tfloat_literal
								| boolean_literal
								| Tstring_literal
								| Tglyph_literal
								;

			boolean_literal		: Ttrue
								| Tfalse ;
			*/

		TypeDescriptor typeDesc = null;
		FlowValue value = null;

		if (intrinsicLiteral.Tint_literal() != null) {
			value = evaluate_Tint_literal(intrinsicLiteral.Tint_literal());
			typeDesc = this.state.getTypeDesc(intrinsicLiteral.Tint_literal());
		} else if (intrinsicLiteral.Tfloat_literal() != null) {
			value = evaluate_Tfloat_literal(intrinsicLiteral.Tfloat_literal());
			typeDesc = this.state.getTypeDesc(intrinsicLiteral.Tfloat_literal());
		} else if (intrinsicLiteral.boolean_literal() != null) {
			value = evaluate(intrinsicLiteral.boolean_literal());
			typeDesc = this.state.getTypeDesc(intrinsicLiteral.boolean_literal());
		} else if (intrinsicLiteral.Tstring_literal() != null) {
			value = evaluate_Tstring_literal(intrinsicLiteral.Tstring_literal());
			typeDesc = this.state.getTypeDesc(intrinsicLiteral.Tstring_literal());
		} else if (intrinsicLiteral.Tglyph_literal() != null) {
			value = evaluate_Tglyph_literal(intrinsicLiteral.Tglyph_literal());
			typeDesc = this.state.getTypeDesc(intrinsicLiteral.Tglyph_literal());
		} else {
			throwLinePosExc(intrinsicLiteral, "Unrecognized production");
		}

		this.state.setTypeDesc(typeDesc);
		if (value != null) this.state.setVal(intrinsicLiteral, value, typeDesc);

		return value;
	}

	private FlowValue evaluate(Boolean_literalContext boolLiteral) {
		/* boolean_literal	: Ttrue
							| Tfalse ;
			*/
		TypeDescriptor typeDesc = new IntrinsicTypeDescriptor(FlowBoolean);
		FlowValue value = null;

		if (boolLiteral.Ttrue() != null) {
			value = new FlowBooleanValue(true);
		} else {
			value = new FlowBooleanValue(false);
		}

		this.state.setTypeDesc(boolLiteral, typeDesc);
		if (value != null) this.state.setVal(boolLiteral, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Array_literalContext arrayLiteral) {

		/* array_literal : Tl_sqbr expr_seq? Tr_sqbr ; */

		Expr_seqContext exprSeq = arrayLiteral.expr_seq();
		int size = (exprSeq == null ? 0 : exprSeq.expr().size());

		// Identify the common type of the elements:
		FlowTypeDescriptor eltTypeDesc = null;
		boolean allElementsAreStatic = true;
		FlowValue[] eltValues;
		if (exprSeq == null) { // a zero length array literal
			eltValues = new FlowValue[0];
			this.state.addTypeAmbiguousArrayLiteral(arrayLiteral);  // element type must be set in a later pass
		} else {
			eltValues = new FlowValue[exprSeq.size()];
			for (ExprContext expr : exprSeq) {
				eltValue[i] = evaluate(expr);
				if (v == null) allElementsAreStatic = false;

				TypeDesc vTypeDesc = this.state.getTypeDesc(expr);
				if (vTypeDesc == null) return null;
				eltTypeDesc = vTypeDesc.getCommonTypeDesc(eltTypeDesc); // needs to work for arrays,
					// in that the element types must be compatible, but not necessarily the ranges.
					// Ranges are reconciled in a separate pass.
				if (eltTypeDesc == null) {
					addLinePosExc(expr, "No common type found for array literal elements");
					return null;
				}
			}
		}

		FlowValue value = null;
		if (eltValues != null) value = new FlowArrayValue(eltValues);
		ArrayTypeDescriptor typeDesc;

		if (exprSeq == null) {
			typeDesc = new ArrayTypeDescriptor(eltTypeDesc, 0);
		} else {
			typeDesc = new ArrayTypeDescriptor(eltTypeDesc, eltValues.length);
		}

		this.state.setTypeDesc(arrayLiteral, typeDesc);
		if (value != null) this.state.setVal(arrayLiteral, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Logic_exprContext logicExpr) {

		/* logic_expr : comp_expr ( logic_op logic_expr )? ; */

		Comp_exprContext compExpr = logicExpr.comp_expr();
		Logic_opContext logicOp = logicExpr.logic_op();
		Logic_exprContext nestedLogicExpr = logicExpr.logic_expr();

		FlowValue value = null;
		TypeDescriptor typeDesc = null;

		FlowValue compValue = evaluate(compExpr);

		if (nestedLogicExpr == null) {
			value = compValue;
			typeDesc = this.state.getTypeDesc(compExpr);
		} else {
			FlowValue nestedExprValue = evaluate(nestedLogicExpr);

			/* Verify that compExpr and nestedLogicExpr are both logical types: */

			TypeDescriptor<FlowValueType> compTypeDesc = this.state.getTypeDesc(compExpr);
			if (compTypeDesc == null) return null;

			if (compTypeDesc.getType() != FlowBoolean) {
				addLinePosExc(compExpr, "Expected a boolean type");
				return null;
			}

			TypeDescriptor<FlowValueType> nestedExprTypeDesc = this.state.getTypeDesc(nestedLogicExpr);
			if (nestedExprTypeDesc == null) return null;

			if (nestedExprTypeDesc.getType() != FlowBoolean) {
				addLinePosExc(nestedLogicExpr, "Expected a boolean type");
				return null;
			}

			typeDesc = new IntrinsicTypeDescriptor(FlowBoolean);

			/* If both operands are static, then perform the logical operation: */

			if ((compValue != null) && (nestedExprValue != null)) {

				if (logicOp.Tand() != null) {
					value = compValue.value && nestedExprValue.value;
				} else if (logicOp.Tor() != null) {
					value = compValue.value || nestedExprValue.value;
				} else if (logicOp.Txor() != null) {
					value = (compValue.value && (! nestedExprValue.value)) ||
						((! compValue.value) && nestedExprValue.value);
				} else throwLinePosExc(logicOp, "Unrecognized production");
			}
		}

		this.state.setTypeDesc(logicExpr, typeDesc);
		if (value != null) this.state.setVal(logicExpr, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Comp_exprContext compExpr) {

		/* comp_expr : Tnot? num_expr ( comp_op comp_expr )? ; */

		TerminalNode notOp = compExpr.Tnot();
		Num_exprContext numExpr = compExpr.num_expr();
		Comp_opContext compOp = compExpr.comp_op();
		Comp_exprContext nestedCompExpr = compExpr.comp_expr();

		FlowValue numExprValue = evaluate(numExpr);
		TypeDescriptor numExprTypeDesc = this.state.getTypeDesc(numExpr);

		FlowValue value = null;
		TypeDescriptor<FlowValueType> typeDesc = null;

		if (nestedCompExpr == null) {
			value = numExprValue;
			typeDesc = numExprTypeDesc;

		} else { // a boolean operation

			/* Evaluate second operand: */

			FlowValue nestedCompValue = evaluate(nestedCompExpr);
			TypeDescriptor nestedCompTypeDesc = this.state.getTypeDesc(nestedCompExpr);
			typeDesc = new IntrinsicTypeDescriptor(FlowBoolean);

			/* Check that both operands are of the required types for the operation.
				Ref LRM, "Comparison Operators": */

			if ((numExprTypeDesc == null) || (nestedCompTypeDesc == null)) return null;

			FlowDataType op1Type = numExprTypeDesc.getType();
			FlowDataType op2Type = nestedCompTypeDesc.getType();

			switch (compOp.getSymbol().getType()) {
				/* Operands may be of any type: */
				case FlowLexer.Teq:
					break;
				/* Verify that operands are both numeric, glyph, or enum: */
				default:
					if (op1Type == FlowEnum) {
						if (! (numExprTypeDesc.equals(nestedCompTypeDesc))) {
							addLinePosExc(compOp, "When comparing enums, both operands must be the smae type");
							return null;
						}
					} else if (op1Type == FlowGlyph) {
						if (op1Type != op2Type) {
							addLinePosExc(compOp, "A glyph may only be compared with another glyph");
							return null;
						}
					} else if (op1Type.isNumeric() && op2Type.isNumeric()) {
						// ok
					} else {
						addLinePosExc(compOp, "Operands must be enum, glyph, or numeric");
						return null;
					}
			}

			/* Attempt static evaluation: */

			if ((numExprValue != null) && (nestedCompValue != null)) { // compute static value

				// TBD
			}
		}

		if (notOp != null) {

			// Verify that type is boolean:
			if (typeDesc != null) {
				if (typeDesc.getType() != FlowBoolean) {
					addLinePosExc(notOp, "Boolean operator but expression is not boolean");
					return null;
				}

				// Evaluate statically if possible:
				if (value != null) value.value = ! value.value;
			}
		}

		this.state.setTypeDesc(compExpr, typeDesc);
		if (value != null) this.state.setVal(compExpr, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Num_exprContext numExpr) {

		/*	num_expr		: mult_expr ( add_op num_expr )? ;
			add_op			: Tplus | Tminus ;
			*/

		Num_exprContext multExpr = numExpr.mult_expr();
		Add_opContext addOp = numExpr.add_op();
		Num_exprContext nestedNumExpr = numExpr.num_expr();

		FlowValue multExprValue = evaluate(multExpr);
		TypeDescriptor multExprTypeDesc = this.state.getTypeDesc(multExpr);

		FlowValue value = null;
		TypeDescriptor<FlowValueType> typeDesc = null;

		if (nestedNumExpr == null) {
			value = nultExprValue;
			typeDesc = multExprTypeDesc;

		} else {
			/* Perform type checking. Both operands must be numeric; or, for addition, both
				must be automatically convertible to string. Ref. LRM, "Scalar Arithmetic Operators". */

			FlowValue nestedNumExprValue = evaluate(nestedNumExpr);
			TypeDescriptor nestedNumExprTypeDesc = this.state.getTypeDesc(nestedNumExpr);

			if (multExprTypeDesc == null) return null;
			if (nestedNumExprTypeDesc ==  null) return null;

			FlowDataType op1Type = multExprTypeDesc.getType();
			FlowDataType op2Type = nestedNumExprTypeDesc.getType();

			if (op1Type.isNumeric() && op2Type.isNumeric()) {
				/* From LRM, "Scalar Arithmetic Operators":
					If the operands are of the same type, the result is also of that type...
					If one of the operands is a long float, the result is a long float.
					Otherwise, automatic conversion must be permitted to bring the two types
					to the same type: if not, it is an error.
					*/

				switch (addOp.getSymbol().getType()) {
					case FlowLexer.Tplus:
						// fall through
					case FlowLexer.Tminus:
						if (op1Type == op2Type) {
							typeDesc = new IntrinsicTypeDescriptor(op1Type);
						} else if ((op1Type == FlowLongFloat) || (op2Type == FlowLongFloat)) {
							typeDesc = new IntrinsicTypeDescriptor(FlowLongFloat);

							// Attribute with type conversion annotations:
							this.state.setTypeConvAnnot(multExpr, new TypeConverstionAnnotation(multExpr,
								op1Type, FlowLongFloat));
							this.state.setTypeConvAnnot(nestedNumExpr, new TypeConverstionAnnotation(nestedNumExpr,
								op2Type, FlowLongFloat));

						} else {
							FlowDataType commonType = FlowDataType.findCommonIntrinsicType(op1Type, op2Type);
							if (commonType == null) {
								addLinePosExc(addOp, "No implicit type conversion between these types");
								return null;
							}
							typeDesc = new IntrinsicTypeDescriptor(commonType);

							// Attribute with type conversion annotations:
							this.state.setTypeConvAnnot(multExpr, new TypeConverstionAnnotation(multExpr,
								op1Type, commonType));
							this.state.setTypeConvAnnot(nestedNumExpr, new TypeConverstionAnnotation(nestedNumExpr,
								op2Type, commonType));
						}
						break;
					default:
						throwLinePosExc(addOp, "Unrecognized production");
				}

			} else if ( // both are convertible to string && op is Tplus
					(addOp.getSymbol().getType() == FlowLexer.Tplus) &&
					op1Type.automaticConversionToAllowed(FlowString) &&
					op2Type.automaticConversionToAllowed(FlowString)) {
				typeDesc = new IntrinsicTypeDescriptor(FlowString);
			} else {
				addLinePosExc(addOp, "Operands must be both numeric or both string or glyph");
				return null;
			}
		}

		this.state.setTypeDesc(numExpr, typeDesc);
		if (value != null) this.state.setVal(numExpr, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Mult_exprContext multExpr) {

		/*	mult_expr	: exp_expr ( mult_op mult_expr )? ;
			mult_op		: Tasterisk | Tslash ;
			*/

		Exp_exprContext expExpr = multExpr.exp_expr();
		Mult_opContext multOp = multExpr.mult_op();
		Mult_exprContext nestedMultExpr = multExpr.mult_expr();

		FlowValue expExprValue = evaluate(expExpr);
		TypeDescriptor expExprTypeDesc = this.state.getTypeDesc(expExpr);

		FlowValue value = null;
		TypeDescriptor<FlowValueType> typeDesc = null;

		if (multOp == null) {
			value = expExprValue;
			typeDesc = expExprTypeDesc;
		} else {

			nestedMultExprValue = evaluate(nestedMultExpr);
			TypeDescriptor nestedMultExprTypeDesc = this.state.getTypeDesc(nestedMultExpr);

			/* Check that both operands are numeric: */

			FlowDataType op1Type = expExprTypeDesc.getType();
			FlowDataType op2Type = nestedMultExprTypeDesc.getType();

			if (! (op1Type.isNumeric() && op2Type.isNumeric())) {
				addLinePosExc(multOp, "Both operands must be numeric");
				return null;
			}

			/* From LRM, "Scalar Arithmetic Operators":
				If the operands are of the same type, the result is also of that type...
				If one of the operands is a long float, the result is a long float.
				Otherwise, automatic conversion must be permitted to bring the two types
				to the same type: if not, it is an error.
				*/
			switch (multOp.getSymbol().getType()) {

				case FlowLexer.Tslash: /* For division, if both operands are integral types,
					the result is a float. If one of the operands is a long float, the result is a
					long float. Otherwise, automatic conversion must be permitted to bring the two types
					to the same type: if not, it is an error. */

					if (op1Type.isIntegral() && op2Type.isIntegral()) { // both operands are integral
						typeDesc = new IntrinsicTypeDescriptor(FlowFloat);
						break;
					}

					// fall through

				case FlowLexer.Tasterisk:

					if (op1Type == op2Type) {
						typeDesc = new IntrinsicTypeDescriptor(op1Type);
					} else if ((op1Type == FlowLongFloat) || (op2Type == FlowLongFloat)) {
						typeDesc = new IntrinsicTypeDescriptor(FlowLongFloat);

						this.state.setTypeConvAnnot(expExpr, new TypeConverstionAnnotation(expExpr,
							op1Type, FlowLongFloat));
						this.state.setTypeConvAnnot(nestedMultExpr, new TypeConverstionAnnotation(nestedMultExpr,
							op2Type, FlowLongFloat));
					} else {
						FlowDataType commonType = FlowDataType.findCommonIntrinsicType(op1Type, op2Type);
						if (commonType == null) {
							addLinePosExc(addOp, "No implicit type conversion between these types");
							return null;
						}
						typeDesc = new IntrinsicTypeDescriptor(commonType);
						this.state.setTypeConvAnnot(expExpr, new TypeConverstionAnnotation(expExpr,
							op1Type, commonType));
						this.state.setTypeConvAnnot(nestedMultExpr, new TypeConverstionAnnotation(nestedMultExpr,
							op2Type, commonType));
					}

					break;

				default:
					throwLinePosExc(multOp, "Unrecognized production");
			}
		}

		this.state.setTypeDesc(multExpr, typeDesc);
		if (value != null) this.state.setVal(multExpr, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Exp_exprContext expExpr) {

		/* exp_expr : unop_expr ( Tcaret exp_expr )? ; */

		Unop_exprContext unopExpr = expExpr.unop_expr();
		TerminalNode caret = expExpr.Tcaret();
		Exp_exprContext nestedExpExpr = expExpr.exp_expr();

		FlowValue unopExprValue = evaluate(unopExpr);
		TypeDescriptor unopExprTypeDesc = this.state.getTypeDesc(unopExpr);

		FlowValue value = null;
		TypeDescriptor<FlowValueType> typeDesc = null;

		if (caret == null) {
			value = unopExprValue;
			typeDesc = unopExprTypeDesc;
		} else {

			FlowValue nestedExpExprValue = evaluate(nestedExpExpr);
			TypeDescriptor nestedExpExprTypeDesc = this.state.getTypeDesc(nestedExpExpr);

			/* From LRM, "Scalar Arithmetic Operators":
				In an exponentiation, if the exponent is an int type, then an implementation
				_may_ carry out the exponentiation through repeated multiplication. If the exponent
				is a floating point type, then it is carried out as if it had been specified with
				the exp() function in namespace flow.Standard.
				*/

			FlowDataType op1Type = unopExprTypeDesc.getType();
			FlowDataType op2Type = nestedExpExprTypeDesc.getType();

			if (! (op1Type.isNumeric() && op2Type.isNumeric())) {
				addLinePosExc(caret, "Both operands must be numeric");
				return null;
			}

			/* From LRM, "Scalar Arithmetic Operators":
				If the operands are of the same type, the result is also of that type...
				If one of the operands is a long float, the result is a long float.
				Otherwise, automatic conversion must be permitted to bring the two types
				to the same type: if not, it is an error.
				*/

			if (op1Type == op2Type) {
				typeDesc = new IntrinsicTypeDescriptor(op1Type);
			} else if ((op1Type == FlowLongFloat) || (op2Type == FlowLongFloat)) {
				typeDesc = new IntrinsicTypeDescriptor(FlowLongFloat);
				this.state.setTypeConvAnnot(unopExpr, new TypeConverstionAnnotation(unopExpr,
					op1Type, FlowLongFloat));
				this.state.setTypeConvAnnot(nestedExpExpr, new TypeConverstionAnnotation(nestedExpExpr,
					op2Type, FlowLongFloat));
			} else {
				FlowDataType commonType = FlowDataType.findCommonIntrinsicType(op1Type, op2Type);
				if (commonType == null) {
					addLinePosExc(caret, "No implicit type conversion between these types");
					return null;
				}
				typeDesc = new IntrinsicTypeDescriptor(commonType);
				this.state.setTypeConvAnnot(unopExpr, new TypeConverstionAnnotation(unopExpr,
					op1Type, commonType));
				this.state.setTypeConvAnnot(nestedExpExpr, new TypeConverstionAnnotation(nestedExpExpr,
					op2Type, commonType));
			}

		}

		this.state.setTypeDesc(expExpr, typeDesc);
		if (value != null) this.state.setVal(expExpr, value, typeDesc);
		return value;
	}

	private FlowValue evaluate(Unop_exprContext unopExpr) {

		/*	unop_expr	: value inc_op? ;
			inc_op		: Tinc | Tdec ;
			*/

		ValueContext value = unopExpr.value();
		Inc_opContext incOp = unopExpr.inc_op();

		FlowValue valueValue = evaluate(value);
		TypeDescriptor valueTypeDesc = this.state.getTypeDesc(value);

		FlowValue result = null;
		TypeDescriptor<FlowValueType> typeDesc = null;

		if (incOp == null) {
			value = valueValue;
			typeDesc = valueTypeDesc;
		} else {
			FlowDataType opType = valueTypeDesc.getType();

			if (! (opType.isNumeric())) {
				addLinePosExc(incOp, "Operand must be numeric");
				return null;
			}

			typeDesc = valueTypeDesc;

			/* TO DO: Attempt static evaluation: */
		}

		this.state.setTypeDesc(unopExpr, typeDesc);
		if (result != null) this.state.setVal(new ExprAnnotation(unopExpr, result, typeDesc));
		return result;
	}



	/*
	 Helper methods:
	 */


	/**
	 Find the next outer name scope node that encloses the specified node, and return the enclosing
	 node's NameScope. If none, return null.
	 */
	private NameScope<FlowValueType, ParseTree, TerminalNode> getEnclosingScope(ParseTree node) {

		if (node == null) return null;

		for (;;) {
			ParseTree parent = node.getParent();
			if (parent == null) return null;

			NameScope<FlowValueType, ParseTree, TerminalNode> scope = this.state.getScope(node);
			if (scope != null) return scope;

			node = parent;
		}
	}

	/**
	 Find and return the name scope of the class in which the specified node is instantiated.
	 If there is no enclosing class, return null.
	 */
	private NameScope<FlowValueType, ParseTree, TerminalNode> getEnclosingClassScope(ParseTree node) {

		NameScope<FlowValueType, ParseTree, TerminalNode> scope;
		for (;;) {
			scope = getEnclosingScope(node);
			if (scope == null) return null;
			ParseTree defNode = scope.getNodeThatDefinesScope();
			if (defNode == null) return null;
			if (defNode instanceof Class_defContext) return scope;
			node = defNode;
		}
	}

	/**
	 Return true if the specified symbol table entry is within the hierarchy of name scopes rooted
	 at the specified name scope. Do not include appended scopes in the determination.
	 */
	private boolean isWithinScopeHierarchy(SymbolEntry<FlowValueType, ParseTree, TerminalNode> entry,
		NameScope<FlowValueType, ParseTree, TerminalNode> enclScope) {

		for (;;) { // each enclosing scope, working upward

			if (entry.getEnclosingScope() == scope) { // entry belongs to the scope
				return true;
			}

			scope = scope.getParentNameScope();
			if (scope == null) return false;
			if (scope == enclScope) return false;
		}
	}

	private static final BigDecimal MaxByteValue = new BigDecimal();
	private static final BigDecimal MinNegIntValue = new BigDecimal();
	private static final BigDecimal MaxIntValue = new BigDecimal();
	private static final BigDecimal MaxUnsignedIntValue = new BigDecimal();
	private static final BigDecimal MinNegLongValue = new BigDecimal();
	private static final BigDecimal MaxLongValue = new BigDecimal();
	private static final BigDecimal MaxUnsignedLongValue = new BigDecimal();

	/**
	 Parse an int literal and attempt to represent it as a Flow value, using the
	 most compact form possible: byte, unsigned byte, int, unsigned int, long int, long unsigned int.
	 Note that the magnitude/precision of each of these Flow types is implementation specific
	 (ref. LRM, "Intrinsic Types").
	 */
	private FlowValue evaluate_Tint_literal(TerminalNode intLiteral) {

		/* Tint_literal : [+\-]? Twhole_number_literal ;
			*/

		// Obtain string representation in a format acceptable to BigDecimal:
		String str = intLiteral.getSymbol().getText();
		BigDecimal decVal = new BigDecimal(str);

		// Determine the most compact Flow type that can represent the value:

		FlowDataType valueType;

		if (decVal.compareTo(MaxByteValue) <= 0) {
			// fits in (unsigned) byte
			valueType = FlowByte;
		}
		else if ((decVal.compareTo(MinNegIntValue) >= 0) || (decVal.compareTo(MaxIntValue) <= 0)) {
			// fits in signed int
			valueType = FlowInt;
		}
		else if (decVal.compareTo(MaxUnsignedIntValue) <= 0) {
			// fits in unsigned int
			valueType = FlowUnsignedInt;
		}
		else if ((decVal.compareTo(MinNegLongValue) >= 0) || (decVal.compareTo(MaxLongValue) <= 0)) {
			// fits in signed long
			valueType = FlowLongInt;
		}
		else if (decVal.compareTo(MaxUnsignedLongValue) <= 0) {
			// fits in unsigned long
			valueType = FlowUnsigndLongInt;
		}
		else
			throwLinePosExc(intLiteral, "Unexpected numeric case");

		TypeDescriptor typeDesc = new IntrinsicTypeDescriptor(valueType);
		this.state.setTypeDesc(intLiteral, typeDesc);
		if (value != null) this.state.setVal(new ExprAnnotation(intLiteral, value, typeDesc));
		return value;
	}

	private FlowValue evaluate_Tfloat_literal(TerminalNode floatLiteral) {

		/* Tfloat_literal : Tint_literal '.' Twhole_number_literal ( [eEdD] Tint_literal )? ;
			*/

		ParseTree base = floatLiteral.getChild(0);
		if (! (base instanceof TerminalNode)) throwLinePosExc(base, "Unexpected production");
		ParseTree fraction = floatLiteral.getChild(2);
		if (! (fraction instanceof TerminalNode)) throwLinePosExc(fraction, "Unexpected production");

		FlowValue baseValue = evaluate_Tint_literal((TerminalNode)base);
		FlowValue fractionValue = evaluate_Tint_literal((TerminalNode)fraction);

		FlowValue value = null;
		TypeDescriptor typeDesc = null;

		if (floatLiteral.getChildCount() > 3) { // has an exponent
			eSymbol = floatLiteral.getChild(3);

			String eText = eSymbol.getText();
			boolean doublePrecision = false;
			if (eText.equals("e") || eText.equals("E")) {
				// no op
			} else if (eText.equals("d") || eText.equals("D")) {
				doublePrecision = true;
			} else throwLinePosExc(eSymbol, "Unexpected procuction");

			// Parse exponent value:
			ParseTree exponent = floatLiteral.getChild(4);
			if (! (exponent instanceof TerminalNode)) throwLinePosExc(exponent, "Unexpected production");
			FlowValue exponentValue = evaluate_Tint_literal((TerminalNode)exponent);

			if (doublePrecision) {

				double doubleValue;
				try {
					doubleValue = Double.parseDouble(base.getText() + "." + fraction.getText() + "E" + exponent.getText());
				} catch (NumberFormatException ex) {
					throwLinePosExc(floatLiteral, "Unable to parse double precision value; " + ex.getMessage());
				}

				value = new FlowLongFloatValue(doubleValue);
				typeDesc = new IntrinsicTypeDescriptor(FlowLongFloat);

			} else {

				float floatValue;
				try {
					floatValue = Float.parseFloat(base.getText() + "." + fraction.getText() + "E" + exponent.getText());
				} catch (NumberFormatException ex) {
					throwLinePosExc(floatLiteral, "Unable to parse float value; " + ex.getMessage());
				}

				value = new FlowFloatValue(floatValue);
				typeDesc = new IntrinsicTypeDescriptor(FlowFloat);
			}

		} else {
			float floatValue;
			try {
				floatValue = Float.parseFloat(base.getText() + "." + fraction.getText());
			} catch (NumberFormatException ex) {
				throwLinePosExc(floatLiteral, "Unable to parse float value; " + ex.getMessage());
			}
			value = new FlowFloatValue(floatValue);
			typeDesc = new IntrinsicTypeDescriptor(FlowFloat);
		}

		this.state.setTypeDesc(floatLiteral, typeDesc);
		if (value != null) this.state.setVal(new ExprAnnotation(floatLiteral, value, typeDesc));
		return value;
	}

	private FlowValue evaluate_Tstring_literal(TerminalNode stringLiteral) {

		/*	Tstring_literal		: Tquote Tstring_element* Tquote ;
			Tstring_element		: Tascii_char_seq | Tunicode_spec | Tescape_seq ;
			Tascii_char_seq		: Tascii_char+ ;
			Tescape_seq			: '\\' Tescaped_char ;
			Thex_chars			: [a-fA-F0-9]+ ;
			Tunicode_spec		: '%' 'u' Tl_paren Thex_chars Tr_paren ;
			*/

		/* Handle the escape syntax. Ref. LRM, "Non-Ascii In Strings": */

		List<TerminalNode> stringElts = stringLiteral.Tstring_element();

		String stringValue = "";
		for (TerminalNode stringElt : stringElts) {
			TerminalNode glyphCharSeq = stringElt.Tascii_char_seq();
			TerminalNode unicodeSpec = stringElt.Tunicode_spec();
			TerminalNode escapeSeq = stringElt.Tescape_seq();

			if (glyphCharSeq != null) {
				stringValue += glyphCharSeq.getText();
			} else if (unicodeSpec != null) {
				int[] codePoint = new int[1];
				try {
					codePoint[0] = interpretUnicodeSpec(unicodeSpec);
				} catch (Exception ex) {
					addLinePosExc(unicodeSpec, ex.getMessage());
					return null;
				}

				String s;
				try {
					s = new String(codePoint, 0, 1);
				} catch (IllegalArgumentException ex) {
					addLinePosExc(unicodeSpec, ex.getMessage());
					return null;
				}

				stringValue += s;

			} else if (escapeSeq != null) {

				char c = interpretEscapeSeq(escapeSeq);
				stringValue += c;

			} else throwLinePosExc(stringElt, "Unrecognized production");
		}

		TypeDescriptor typeDesc = new IntrinsicTypeDescriptor(FlowString);
		FlowValue value = new FlowStringValue(stringValue);

		this.state.setTypeDesc(stringLiteral, typeDesc);
		if (value != null) this.state.setVal(new ExprAnnotation(stringLiteral, value, typeDesc));
		return value;
	}

	private FlowValue evaluate_Tglyph_literal(TerminalNode glyphLiteral) {

		/*	Tglyph_literal		: Tapos ( Tascii_char | Tunicode_spec | Tescape_seq ) Tapos ;
			Tescaped_char		: [nrt"'\\] ;
			Tescape_seq			: '\\' Tescaped_char ;
			Thex_chars			: [a-fA-F0-9]+ ;
			Tunicode_spec		: '%' 'u' Tl_paren Thex_chars Tr_paren ;
			*/

		/* Interpret char_spec. Ref. LRM, "Non-Ascii In Strings", "Intrinsic Types", "Glyph". */

		TerminalNode glyphChar = glyphLiteral.Tascii_char();
		TerminalNode unicodeSpec = glyphLiteral.Tunicode_spec();
		TerminalNode escapeSeq = glyphLiteral.Tescape_seq();

		char charValue;

		if (glyphChar != null) { // just an ascii character
			charValue = glyphChar.getText();
		} else if (unicodeSpec != null) {

			int codePoint;
			try {
				codePoint = interpretUnicodeSpec(unicodeSpec);
			} catch (Exception ex) {
				addLinePosExc(unicodeSpec, ex.getMessage());
				return null;
			}

			char c = Character.highSurrogate(intValue);
			if (Character.isHighSurrogate(c)) {
				char lowSur = lowSurrogate(intValue);
				if (! isLowSurrogate(lowSur)) {
					addLinePosExc(hexChars, "Illegal Unicode value: invalid low surrogate");
					return null;
				}
				value = FlowGlyphValue(c, lowSur);
			} else {
				value = FlowGlyphValue(c);
			}

		} else if (escapeSeq != null) {

			char c = interpretEscapeSeq(escapeSeq);
			value = new FlowGlyphValue(c);

		} else throwLinePosExc(glyphLiteral, "Unrecognized production");

		TypeDescriptor typeDesc = new IntrinsicTypeDescriptor(FlowGlyph);
		FlowValue value = new FlowGlyphValue(charValue);

		this.state.setTypeDesc(glyphLiteral, typeDesc);
		if (value != null) this.state.setVal(new ExprAnnotation(glyphLiteral, value, typeDesc));
		return value;
	}

	private int interpretUnicodeSpec(TerminalNode unicodeSpec) throws Exception {

		TerminalNode hexChars = unicodeSpec.Thex_chars();

		/* Interpret the sequence of hex characters: */
		BigInteger bigInteger;
		try {
			bigInteger = new BigInteger(hexChars.getText(), 16);
		} catch (NumberFormatException ex) {
			throw new Exception("Ill-formatted hexadecimal number");
		}

		/* Attempt to map the value to a Unicode glyph.
			Ref: https://docs.oracle.com/javase/8/docs/api/java/lang/Character.html
			*/

		int intValue;
		try {
			intValue = bigInteger.intValueExact();
		} catch (ArithmeticException ex) {
			throw new Exception("Value out of range");
		}

		return intValue;
	}

	private char interpretEscapeSeq(TerminalNode escapeSeq) {

		TereminalNode escapedChar = escapeSeq.TescapedChar();
		char c;
		switch (escapedChar.getText()) {
			case "n": c = '\n'; break;
			case "r": c = '\r'; break;
			case "t": c = '\t'; break;
			case "'": c = '\''; break;
			case "\"": c = '"'; break;
			case "\\": c = '\\'; break;
			default: throwLinePosExc(escapedChar, "Unexpected production");
		}
		return c;
	}
}
