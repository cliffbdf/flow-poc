package cliffberg.flow.analyzer;

import cliffberg.symboltable.Annotation;
import cliffberg.flow.parser.FlowParser.Value_type_specContext;
import org.antlr.v4.runtime.ParserRuleContext;

public class ArrayDimensionAnnotation implements Annotation {

	public Value_type_specContext arrayValueTypeSpec;
	public Long[] dimensions;

	public ArrayDimensionAnnotation(Value_type_specContext arrayValueTypeSpec, Long[] dimensions) {
		this.arrayValueTypeSpec = arrayValueTypeSpec;
		this.dimensions = dimensions;
	}
}
