package cliffberg.flow.analyzer;

import cliffberg.symboltable.ConversionException;
import static cliffberg.utilities.AssertionUtilities.*;

import java.util.List;

/**
 Defines the expression value types that are permitted by the Flow language.
 */
public enum FlowDataType implements FlowValueType {
	Unknown,
	FlowByte,
	FlowInt,
	FlowUnsignedInt,
	FlowLongInt,
	FlowUnsignedLongInt,
	FlowFloat,
	FlowLongFloat,
	FlowBoolean,
	FlowGlyph,
	FlowString,
	FlowEnum,
	FlowStruct,
	FlowArray;

	public boolean isIntrinsic() {
		return (this.ordinal() > Unknown.ordinal()) && (this.ordinal() < FlowEnum.ordinal());
	}

	public boolean isNumeric() {
		return (this.ordinal() >= FlowByte.ordinal()) && (this.ordinal() <= FlowLongFloat.ordinal());
	}

	public boolean isIntegral() {
		return (this.ordinal() >= FlowByte.ordinal()) && (this.ordinal() <= FlowUnsignedLongInt.ordinal());
	}

	public boolean isFloat() {
		return (this.ordinal() >= FlowFloat.ordinal()) && (this.ordinal() <= FlowLongFloat.ordinal());
	}

	/**
	 If the target type or the argument type is not an intrinsic (unstructured) type,
	 or if the types are incompatible, then an error is thrown.
	 */
	public boolean dataLossPossibleWhenConvertingTo(FlowDataType toType) {
		/* Loss can occur when:
			Both types are between FlowByte and FlowUnsignedLongInt, inclusive, and,
				this > toType.
			One type is between FlowInt and FlowUnsignedLongInt, inclusive, and the other
				is between FlowFloat and FlowLongFloat, inclusive.
			Both types are between FlowGlyph and FlowString, inclusive, and this > toType.
		 */
		if (
			this == Unknown ||
			this.ordinal() >= FlowEnum.ordinal()) throw new RuntimeException("This type is not an intrinsic type");

		if (
			toType == Unknown ||
			toType.ordinal() >= FlowEnum.ordinal()) throw new RuntimeException("The argument type is not an intrinsic type");

		if (
				(FlowByte.ordinal() >= this.ordinal() && this.ordinal() <= FlowUnsignedLongInt.ordinal())
			&&	(FlowByte.ordinal() >= toType.ordinal() && toType.ordinal() <= FlowUnsignedLongInt.ordinal())
			) if (this.ordinal() > toType.ordinal())
				return true;
				else return false;

		if (
			(
				(FlowByte.ordinal() >= this.ordinal() && this.ordinal() <= FlowUnsignedLongInt.ordinal())
			&&	(FlowFloat.ordinal() >= toType.ordinal() && toType.ordinal() <= FlowLongFloat.ordinal())
			)
			||
			(
				(FlowByte.ordinal() >= toType.ordinal() && toType.ordinal() <= FlowUnsignedLongInt.ordinal())
			&&	(FlowFloat.ordinal() >= this.ordinal() && this.ordinal() <= FlowLongFloat.ordinal())
			)
			) return true;

		if (
				(FlowGlyph.ordinal() >= this.ordinal() && this.ordinal() <= FlowString.ordinal())
			&&	(FlowGlyph.ordinal() >= toType.ordinal() && toType.ordinal() <= FlowString.ordinal())
			&& this.ordinal() > toType.ordinal()
			) return true;

		return false;
	}

	/**
	 Return the common type that both types can be converted to, losing the least information.
	 Ref. LRM, "Automatic Type Conversions".
	 If there is no common type, then return null. If one of the arguments is null, then
	 return the other argument. If both arguments are null, return null. If one or both of
	 the arguments is Unknown, return null. If one of the types is not intrinsic
	 then return null.
	 */
	public static FlowDataType findCommonIntrinsicType(FlowDataType aType, FlowDataType otherType) {
		/*
			If both types are the same, return that type.
			If both types are integral, choose the higher type.
			If both types are float, choose the higher type.
			If one is integral and the other float, choose the float type.
			If one is string and the other glyph, choose string.
			Otherwise, throw an exception.
		 */

		if (! aType.isIntrinsic()) throw new RuntimeException(aType + " is not an intrinsic type");
		if (! otherType.isIntrinsic()) throw new RuntimeException(otherType + " is not an intrinsic type");

		if (aType == otherType) return aType;

		if (aType.isIntegral() && otherType.isIntegral())
			if (aType.ordinal() > otherType.ordinal()) return aType;
			else return otherType;

		if (aType.isFloat() && otherType.isFloat())
			if (aType.ordinal() > otherType.ordinal()) return aType;
			else return otherType;

		if (aType.isIntegral() && otherType.isFloat()) return otherType;
		if (otherType.isIntegral() && aType.isFloat()) return aType;

		if (aType == FlowString && otherType == FlowGlyph) return FlowString;
		if (otherType == FlowString && aType == FlowGlyph) return FlowString;

		throw new RuntimeException("Incompatible types: " + aType + ", " + otherType);
	}

	/**
	 Return true if this Flow type may be automatically (implicitly) converted
	 to the specified Flow type.
	 *
	 Per the LRM ("Automatic Type Conversions"), these are allowed:
		int				-> (long int) or (unsigned int)
		unsigned int	-> (long int) or (unsigned long int)
		float			-> (long float)
		glyph			-> string
	 *
	 */
	public boolean automaticConversionToAllowed(FlowDataType convertTo) {
		if (this == convertTo) return true;
		switch (this) {
			case FlowByte: return false;
			case FlowInt: return convertTo == FlowLongInt || convertTo == FlowUnsignedInt;
			case FlowUnsignedInt: return convertTo == FlowLongInt || convertTo == FlowUnsignedLongInt;
			case FlowLongInt: return false;
			case FlowUnsignedLongInt: return false;
			case FlowFloat: return convertTo == FlowLongFloat;
			case FlowLongFloat: return false;
			case FlowBoolean: return false;
			case FlowGlyph: return convertTo == FlowString;
			case FlowString: return false;
			case FlowEnum: return false;
			case FlowStruct: return false;
			case FlowArray: return false;
			default: throw new RuntimeException("Unrecognized flow value type: " + this);
		}
	}

	/* Methods from interface ValueType: */

	public void checkNativeTypeAssignabilityFrom(Class sourceNativeType) throws ConversionException {
		throw new RuntimeException("Not used, so not implemented");

		/*
		switch (this) {
			case FlowInt: assertThat(Number.class.isAssignableFrom(sourceNativeType),
				"Cannot assign " + sourceNativeType.getName() + " to numeric"); return;

			case FlowInt: // only can be represented as a Java int
			case FlowLongInt:
			case FlowUnsignedInt:
			case FlowUnsignedLongInt:
			case FlowFloat:
			case FlowLongFloat:
			case FlowBoolean:
			case FlowByte:
			case FlowGlyph:
			case FlowString:
			case FlowEnum:
			case FlowStruct:
			case FlowArray:
			default: throw new RuntimeException(
				"Unexpected ValueType value: " + this.toString());
		}
		*/
	}
}
