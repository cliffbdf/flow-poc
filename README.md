# What this is
Proof of concept (POC) and reference implementation compiler and launcher for Flow language.

# Licensing
Flow(TM) language license and trademark information is contained in the Language Reference (see below).
The code in this project is open source, licensed under the MIT license (see the LICENSE file),
except that the language syntax specification (Flow.g4) is licensed as stated in that file.
Other syntax specifications are permitted as long as the language rules, as defined in the
Language Reference, are adhered to. Our goal is to allow and encourage new implementations, but
to maintain control of the language itself, and limit use of the Flow(TM) name to conforming
implementations, to protect the interoperability of different implementations.

Other implementations and derivative implementations are encouraged, as long as the Flow language,
as defined in the Language Reference, are adhered to. Implementations that adhere to the Language
Reference may use the name "Flow" to describe the language that they support. See the Language
Reference for more information. Requests for derivatives of the Flow language or additions to Flow will be
seriously considered, but may not be done (except on an experimental basis) without permission.

# Designs, specifications, and explanations
The following documents define and explain the language and its motivation:

* [Language reference manual](https://drive.google.com/open?id=1fLb-E2AopI60EfXNUCSGlEZjeu0Ku8EA22M7x8MQr_0)
* [Language overview](https://drive.google.com/open?id=1X07MelaCxVg9F4aAw9UYWPBmn9PnxQ88ODZXyGtnUg4)
* [Why another language?](https://drive.google.com/open?id=1CFkzNP-jq4sVHp6hixzayzSrXSeqe2XNOCL9ToraTtg)
* [Analyzer Design](https://docs.google.com/document/d/1Lh4bCMkbofyIIs2KhGDXp8yHaVo3RsnXB6Ek2yKL0lM/edit?usp=sharing)
* [Symbol Table Architecture](https://docs.google.com/presentation/d/1ckD59yy3aQssyUFzdOh0tbGC66pLmx1Q65vimY-HESc/edit?usp=sharing)

# Current status
The compiler acts as a syntax recognizer (parser). The full language syntax
is implemented. Semantic rules are not yet implemented.
A semantic analyzer is under development, with a JVM compatible code generator next: these
will constitute a reference implementation for the JVM runtime.

# Goals of this POC
This POC is intended to be reference implementation that is usable for real work, and
usable for the purpose of verifying the integrity and usability of the Flow language.
It is intended that other implementations either build on this one, or are created from scratch.

## Vision
After the POC, it is envisioned that a production quality toolset
will be implemented using the LLVM framework or GraalVM, with backends for the JVM ecosystem
and also for non-JVM runtimes - in particular, the LLVM intermediate form or GraalVM format are
a candidates for a next generation target, with ultimate runtime targets of iOS, Android,
Web Assembly, Linux, OSX, Windows, and JVM.

# Components
The POC has these components:

* Main program
* Parser
* Analyzer
* Generator

The POC uses,
* [the Antlr parser generation toolkit](https://www.antlr.org/)
* [the Byte Buddy JVM class generation library](https://bytebuddy.net)
* [this](https://gitlab.com/cliffbdf/symboltable) symbol table library
* [this](https://gitlab.com/cliffbdf/utilities-java) utilities library.
* [the jopt-simple option parsing libary](http://jopt-simple.github.io/jopt-simple/)
